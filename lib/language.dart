import 'all_file.dart';

class Languages {
  static Map<String, Map<String, String>> languages = {
    'en': _buildLangEn(),
    'vn': _buildLangVn(),
  };

  static _buildLangEn() {
    return {
      //General
      'LBL_DELETE': 'Delete',
      'LBL_EDIT': 'Edit',
      'LBL_SAVE': 'Save',
      'LBL_CANCEL': 'Cancel',

      // Main
      '': '',
      'LBL_HOME': 'Home',
      'LBL_COMMUNITIES': 'Directory',
      'LBL_CALENDAR': 'Calendar',
      'LBL_CALENDAR_FULL': 'Calendar',

      // Login screen
      'LBL_PROFILE': 'Profile',
      'LBL_USERNAME': 'User name',
      'LBL_PASSWORD': 'Password',
      'LBL_FORGOT_PASS': 'Forgot password',
      'LBL_SIGNUP': 'Sign up',
      'LBL_SIGNIN': 'Sign in',
      'LBL_OR': 'Or',
      'LBL_DONT_HAVE_ACC': "Don't have account?",

      //Signup screen
      'LBL_BACK': 'Back',
      'LBL_REGISTER': 'Register',
      'LBL_HAVE_ACC': 'Already have an account?',
      'LBL_CODE': 'Code',
      'LBL_PHONE': 'Phone',
      'LBL_EMAIL': 'Email',
      'LBL_CONFIRM_PASSWORD': 'Verify password',

      //Forgot password screen
      'LBL_SEND': 'Send',
      'LBL_TITLE': 'Enter your email address to request a password reset.',

      //Contacts
      'LBL_DIRECTORY': 'Directory',
      'LBL_YOUR_CARD': 'Your card',
      'LBL_CONTACTS': 'Contacts',
      'LBL_NONE': 'None',

      //Team
      'LBL_TEAM': 'Team',
      'LBL_CREATE_TEAM': 'Create Team',
      'LBL_EDIT_TEAM': 'Edit Team',
      'LBL_TEAM_NAME': 'Team Name',
      'LBL_TEAM_CODE': 'Team Code',
      'LBL_TEAM_EMAIL': 'Team Email',
      'LBL_TEAM_ADDRESS': 'Team Address',
      'LBL_TEAM_PHONE': 'Team Phone',
      'LBL_TEAM_TIMELIMIT': 'Time Limit',

      //User
      'LBL_USER': 'User',
      'LBL_CREATE_USER': 'Create User',
      'LBL_EDIT_USER': 'Edit User',
      'LBL_EDIT_PROFILE': 'Edit profile',
      'LBL_LOGOUT': 'Logout',
      'LBL_SETTINGS': 'Settings',
      'LBL_FIRST_NAME': 'First name',
      'LBL_LAST_NAME': 'Last name',
      'LBL_FULL_NAME': 'Full name',
      'LBL_DOB': 'Day of birth',
      'LBL_JOB_TITLE': 'Job title',

      //News
      'LBL_NEWS': 'News',
      'LBL_CREATE_NEWS': 'Create News',
      'LBL_EDIT_NEWS': 'Edit News',
      'LBL_NEWS_URL': 'News URL',
      'LBL_NEWS_TITLE': 'News Title',
      'LBL_NEWS_DES': 'News Description',

      //Shiftwork
      'LBL_SHIFTWORK': 'Shiftwork',
      'LBL_CREATE_SHIFTWORK': 'Create Shiftwork',
      'LBL_EDIT_SHIFTWORK': 'Edit Shiftwork',
      'LBL_SHIFTWORK_NAME': 'Shiftwork Name',
      'LBL_BEGIN_TIME': 'Time to Start',
      'LBL_END_TIME': 'Time to Finish',
      'LBL_SELECT_TIME': 'Select Time',
      'LBL_SELECT_SHIFTWORK': 'Select Shiftwork',

      //Management
      'LBL_MANAGE': 'Manage',
      'LBL_NEWS_MANAGE': 'News Manage',
      'LBL_USER_MANAGE': 'User Manage',
      'LBL_SHIFTWORK_MANAGE': 'Shiftwork Manage',
      'LBL_TEAM_MANAGE': 'Team Manage',

      //HINT TEXT
      'LBL_FIRST_NAME_HINT': '',
      'LBL_LAST_NAME_HINT': '',
      'LBL_PHONE_HINT': '',
      'LBL_EMAIL_HINT': '',
      'LBL_JOB_TITLE_HINT': '',
      'LBL_USERNAME_HINT': '',
      'LBL_PASSWORD_HINT': '',
      'LBL_NEWS_URL_HINT': '',
      'LBL_NEWS_TITLE_HINT': '',
      'LBL_NEWS_DES_HINT': '',
      'LBL_SHIFTWORK_NAME_HINT': '',
      'LBL_TEAM_NAME_HINT': '',
      'LBL_TEAM_CODE_HINT': '',
      'LBL_TEAM_EMAIL_HINT': '',
      'LBL_TEAM_ADDRESS_HINT': '',
      'LBL_TEAM_PHONE_HINT': '',
      'LBL_TEAM_TIMELIMIT_HINT': '',

      //
      'LBL_OK': 'Ok',
      'LBL_SIGN_UP_SUCCESS': 'Successful',
      'LBL_SIGN_UP_SUCCESS_NOTI':
          'Registration successful, please login again!',
      'LBL_SEARCH': 'Search',
      'LBL_CHECKIN': 'Checkin',
      'LBL_NOT_EMPTY': 'Not be empty!',
      'LBL_TEAM_IPWIFI': 'IP Wifi',
      'LBL_TEAM_IPWIFI_HINT': '',
      'LBL_ADDRESS': 'Address',
      'LBL_PLEASE_WAIT': 'Please wait...!',
      'LBL_MALFORMED': 'Malformed',
      'LBL_RANGE_CHARACTER': '6 - 16 characters',
      'LBL_UPPERCASE_CHARACTER': 'Uppercase character',
      'LBL_REPORT_DETAIL': 'Report detail',
      'LBL_CHOOSE': 'Choose',
      'LBL_FROM': 'From',
      'LBL_TO': 'To',
      'LBL_NO_DATA': 'No data available',
      'LBL_TOTAL_DAY': 'Total day',
      //Report
      'LBL_REPORT': 'Report',
      'LBL_REPORT_SHIFT': 'Attendance report',
      'LBL_REPORT_SHIFT_DES':
          'Report statistics on employee attendance history.',
      'LBL_CHECKOUT': 'Checkout',
      'LBL_DONE': 'Done',
      'LBL_CHANGE_LANGUAGE': 'Change language',
      'LBL_SUB_CHANGE': 'Which language do you prefer?',
      'LBL_NO': 'No.',
      'LBL_SHIFT': 'Shift',
      'LBL_REGIS_ALL_DAY': 'Register work all day',
      'LBL_REGIS_HALF_DAY': 'Register work half a day',
      'LBL_ATTENDED': 'Attended',
      'LBL_REGIS_NOT_ATTEND': 'Register but not attend',
      'LBL_INFOR_LIST_SHIFT': 'List shift',
      'LBL_NO_DATA_AVAILABLE': 'No data available',
      'LBL_CHANGE_PASSWORD': 'Change password',

      'LBL_CURRENT_PASS': 'Current Password',
      'LBL_NEW_PASS': 'New Password',
      'LBL_CURRENT_PASS_HINT': 'Type current password',
      'LBL_NEW_PASS_HINT': 'Type new password',
      'LBL_CONFIRM_PASS': 'Confirm password',
      'LBL_CONFIRM_PASS_HINT': 'Retype new password',
      'LBL_CURRENT_PASS_ERROR': 'Incorrect current password',
      'LBL_NEW_PASS_ERROR': 'Password must be at least 6 characters',
      'LBL_CONFIRM_PASS_ERROR':
      'Your password and confirmation password do not match',
      'LBL_SIGN_IN_ERROR': 'Sign In Error',
      'LBL_SIGN_IN_ERROR_NOTI': 'Something went wrong!',
      'LBL_COMPARE_PASS_ERROR': 'Do not match the old password',

      'LBL_CHECKED': 'Today you have taken attendance',
      'LBL_IP_NOT_MATCH': 'IP Wifi does NOT match',
      'LBL_SUCCESSFULLY': 'successfully',
      'LBL_FAILURE': 'failure. Please try again later!'

    };
  }

  static _buildLangVn() {
    return {
      'LBL_DELETE': 'Xóa',
      'LBL_EDIT': 'Sửa',
      'LBL_SAVE': 'Lưu',
      'LBL_CANCEL': 'Hủy',

      // Main
      '': '',
      'LBL_HOME': 'Trang chủ',
      'LBL_COMMUNITIES': 'Danh bạ',
      'LBL_CALENDAR': 'Lịch',
      'LBL_CALENDAR_FULL': 'Lịch',

      // Login screen
      'LBL_PROFILE': 'Hồ sơ',
      'LBL_USERNAME': 'Tên đăng nhập',
      'LBL_PASSWORD': 'Mật khẩu',
      'LBL_FORGOT_PASS': 'Quên mật khẩu',
      'LBL_SIGNUP': 'Đăng ký',
      'LBL_SIGNIN': 'Đăng nhập',
      'LBL_OR': 'hoặc',
      'LBL_DONT_HAVE_ACC': "Bạn chưa có tài khoản?",

      //Signup screen
      'LBL_BACK': 'Trở về',
      'LBL_REGISTER': 'Đăng ký',
      'LBL_HAVE_ACC': 'Bạn đã có tài khoản?',
      'LBL_CODE': 'Mã',
      'LBL_PHONE': 'Số điện thoại',
      'LBL_EMAIL': 'Email',
      'LBL_CONFIRM_PASSWORD': 'Xác thực mật khẩu',

      //Forgot password screen
      'LBL_SEND': 'Gửi',
      'LBL_TITLE': 'Nhập email của bạn để xác nhận đặt lại mật khẩu.',

      //Contacts
      'LBL_DIRECTORY': 'Danh bạ',
      'LBL_YOUR_CARD': 'Thẻ của bạn',
      'LBL_CONTACTS': 'Liên hệ',
      'LBL_NONE': 'Không',

      //Team
      'LBL_TEAM': 'Nhóm',
      'LBL_CREATE_TEAM': 'Tạo nhóm',
      'LBL_EDIT_TEAM': 'Sửa nhóm',
      'LBL_TEAM_NAME': 'Tên nhóm',
      'LBL_TEAM_CODE': 'Mã nhóm',
      'LBL_TEAM_EMAIL': 'Email',
      'LBL_TEAM_ADDRESS': 'Địa chỉ nhóm',
      'LBL_TEAM_PHONE': 'Số điện thoại nhóm',
      'LBL_TEAM_TIMELIMIT': 'Thời gian giới hạn',

      //User
      'LBL_USER': 'Tài khoản',
      'LBL_CREATE_USER': 'Tạo tài khoản',
      'LBL_EDIT_USER': 'Sửa tài khoản',
      'LBL_EDIT_PROFILE': 'Sửa hồ sơ',
      'LBL_LOGOUT': 'Đăng xuất',
      'LBL_SETTINGS': 'Cài đặt',
      'LBL_FIRST_NAME': 'Tên',
      'LBL_LAST_NAME': 'Họ',
      'LBL_FULL_NAME': 'Họ và tên',
      'LBL_DOB': 'Ngày sinh',
      'LBL_JOB_TITLE': 'Công việc',

      //News
      'LBL_NEWS': 'Tin tức',
      'LBL_CREATE_NEWS': 'Tạo tin tức',
      'LBL_EDIT_NEWS': 'Sửa tin tức',
      'LBL_NEWS_URL': 'Đường dẫn tin tức',
      'LBL_NEWS_TITLE': 'Tiêu đề',
      'LBL_NEWS_DES': 'Mô tả',

      //Shiftwork
      'LBL_SHIFTWORK': 'Ca làm việc',
      'LBL_CREATE_SHIFTWORK': 'Tạo ca',
      'LBL_EDIT_SHIFTWORK': 'Sửa ca',
      'LBL_SHIFTWORK_NAME': 'Teen ca',
      'LBL_BEGIN_TIME': 'Thời gian bắt đầu',
      'LBL_END_TIME': 'Thời gian kết thúc',
      'LBL_SELECT_TIME': 'Chọn thời gian',
      'LBL_SELECT_SHIFTWORK': 'Chọn ca',

      //Management
      'LBL_MANAGE': 'Quản lý',
      'LBL_NEWS_MANAGE': 'Quản lý tin tức',
      'LBL_USER_MANAGE': 'Quản lý tài khoản',
      'LBL_SHIFTWORK_MANAGE': 'Quản lý ca',
      'LBL_TEAM_MANAGE': 'Quản lý nhóm',

      //HINT TEXT
      'LBL_FIRST_NAME_HINT': 'Nhập tên của bạn',
      'LBL_LAST_NAME_HINT': 'Nhập họ của bạn',
      'LBL_PHONE_HINT': 'Nhập số điện thoại',
      'LBL_EMAIL_HINT': 'Nhập email',
      'LBL_JOB_TITLE_HINT': 'Nhập công việc',
      'LBL_USERNAME_HINT': 'Nhập tên đăng nhập',
      'LBL_PASSWORD_HINT': 'Nhập mật khẩu',
      'LBL_NEWS_URL_HINT': 'Nhập đường dẫn tin tức',
      'LBL_NEWS_TITLE_HINT': 'Nhập tiêu đề',
      'LBL_NEWS_DES_HINT': 'Nhập mô tả',
      'LBL_SHIFTWORK_NAME_HINT': 'Nhập tên ca',
      'LBL_TEAM_NAME_HINT': 'Nhập tên nhóm',
      'LBL_TEAM_CODE_HINT': 'Nhập mã nhóm',
      'LBL_TEAM_EMAIL_HINT': 'Nhập email nhóm',
      'LBL_TEAM_ADDRESS_HINT': 'Nhập địa chỉ nhóm',
      'LBL_TEAM_PHONE_HINT': 'Nhập số điện thoại nhóm',
      'LBL_TEAM_TIMELIMIT_HINT': 'Nhập thời gian giới hạn nhóm',

      'LBL_OK': 'Xác nhận',
      'LBL_SIGN_UP_SUCCESS': 'Đăng ký thành công',
      'LBL_SIGN_UP_SUCCESS_NOTI': 'Đăng ký thành công, hãy đăng nhập lại!',
      'LBL_SEARCH': 'Tìm kiếm',
      'LBL_CHECKIN': 'Bắt đầu ca',
      'LBL_CHECKOUT': 'Kết thúc ca',
      'LBL_DONE': 'Hoàn thành',
      'LBL_NOT_EMPTY': 'Không được để trống!',
      'LBL_TEAM_IPWIFI': 'Địa chỉ IP Wifi',
      'LBL_TEAM_IPWIFI_HINT': 'Nhập địa chỉ IP Wifi',
      'LBL_ADDRESS': 'Địa chỉ',
      'LBL_PLEASE_WAIT': 'Đợi xíu bạn nhé...!',
      'LBL_MALFORMED': 'Sai định dạng',
      'LBL_RANGE_CHARACTER': '6 - 16 ký tự',
      'LBL_UPPERCASE_CHARACTER': 'Viết hoa ký tự',
      'LBL_REPORT_DETAIL': 'Chi tiết báo cáo',
      'LBL_CHOOSE': 'Chọn',
      'LBL_FROM': 'Từ',
      'LBL_TO': 'Đến',
      'LBL_NO_DATA': 'Không có dữ liệu',
      'LBL_TOTAL_DAY': 'Tổng ngày',
      //Report
      'LBL_REPORT': 'Báo cáo',
      'LBL_REPORT_SHIFT': 'Báo cáo chấm công',
      'LBL_REPORT_SHIFT_DES':
          'Báo cáo thống kê lịch sử chấm công của nhân viên',
      'LBL_CHANGE_LANGUAGE': 'Thay đổi ngôn ngữ',
      'LBL_SUB_CHANGE': 'Bạn muốn sử dụng ngôn ngữ nào?',
      'LBL_NO': 'STT',
      'LBL_SHIFT': 'Ca',
      'LBL_REGIS_ALL_DAY': 'Đăng ký làm cả ngày',
      'LBL_REGIS_HALF_DAY': 'Đăng ký làm nửa ngày',
      'LBL_ATTENDED': 'Đã điểm danh',
      'LBL_REGIS_NOT_ATTEND': 'Đăng ký nhưng không điểm danh',
      'LBL_INFOR_LIST_SHIFT': 'Lịch làm việc',
      'LBL_NO_DATA_AVAILABLE': 'Không có dữ liệu',
      'LBL_CHANGE_PASSWORD': 'Đổi mật khẩu',
      // Change password
      'LBL_CURRENT_PASS': 'Mật khẩu cũ',
      'LBL_NEW_PASS': 'Mật khẩu mới',
      'LBL_CONFIRM_PASS': 'Mật khẩu xác nhận',
      'LBL_CURRENT_PASS_ERROR': 'Mật khẩu hiện tại không đúng',
      'LBL_NEW_PASS_ERROR': 'Mật khẩu mới ít nhất 6 ký tự ',
      'LBL_CONFIRM_PASS_ERROR': 'Mật khẩu xác nhận không khớp',
      'LBL_CONFIRM_PASS_HINT': 'Nhập lại mật khẩu mới',
      'LBL_CURRENT_PASS_HINT': 'Nhập mật khẩu hiện tại',
      'LBL_NEW_PASS_HINT': 'Nhập mật khẩu mới',
      'LBL_SIGN_IN_ERROR': 'Đăng nhập lỗi',
      'LBL_SIGN_IN_ERROR_NOTI': 'Vui lòng thử lại sau!',
      'LBL_COMPARE_PASS_ERROR': 'Không được trùng với mật khẩu cũ',
      'LBL_CHECKED': 'Hôm nay bạn đã điểm danh',
      'LBL_IP_NOT_MATCH': 'Địa chỉ Wifi không đúng',
      'LBL_SUCCESSFULLY': 'thành công',
      'LBL_FAILURE': 'thất bại. Vui lòng thử lại sau!'

    };
  }

  static String get(String key) {
    return languages[appState.getLanguage][key] ?? key;
  }
}
