import '../../all_file.dart';

part 'news_model.g.dart';
@JsonSerializable()
class NewsModel {
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'title')
  String title;
  @JsonKey(name: 'description')
  String description;
  @JsonKey(name: 'url')
  String url;
  @JsonKey(name: 'imgUrl')
  String imgUrl;
  @JsonKey(name: 'createBy')
  String createBy;
  @JsonKey(name: 'team_id')
  String team_id;

  NewsModel(
      this.id,
      this.title,
      this.description,
      this.url,
      this.imgUrl,
      this.createBy,
      this.team_id,
      );

  factory NewsModel.fromJson(Map<String, dynamic> json) =>
      _$NewsModelFromJson(json);
}
