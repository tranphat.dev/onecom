// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'team_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TeamModel _$TeamModelFromJson(Map<String, dynamic> json) {
  return TeamModel(
    json['id'] as String,
    json['teamName'] as String,
    json['teamCode'] as String,
    json['email'] as String,
    json['phone'] as String,
    json['address'] as String,
    json['timeLimit'] as String,
    json['ipWifi'] as String,
//    json['isWorkOnSat'] as String,
  );
}

Map<String, dynamic> _$TeamModelToJson(TeamModel instance) => <String, dynamic>{
  'id': instance.id,
  'teamName': instance.teamName,
  'teamCode': instance.teamCode,
  'email': instance.email,
  'phone': instance.phone,
  'address': instance.address,
  'timeLimit': instance.timeLimit,
  'ipWifi': instance.ipWifi,
//  'isWorkOnSat': instance.isWorkOnSat,
};
