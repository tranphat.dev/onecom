import '../../all_file.dart';

part 'report_detail_model.g.dart';

@JsonSerializable()
class ReportDetailModel {
  @JsonKey(name: 'fullName')
  String fullName;
  @JsonKey(name: 'totalDay')
  double totalDay;
  @JsonKey(name: 'dateOfBirth')
  String dateOfBirth;
  @JsonKey(name: 'jobTitle')
  String jobTitle;

  ReportDetailModel(
      this.dateOfBirth, this.fullName, this.jobTitle, this.totalDay);

  factory ReportDetailModel.fromJson(Map<String, dynamic> json) =>
      _$ReportDetailModelFromJson(json);
}
