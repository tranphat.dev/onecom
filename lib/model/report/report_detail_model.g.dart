// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportDetailModel _$ReportDetailModelFromJson(Map<String, dynamic> json) {
  return ReportDetailModel(
    json['dateOfBirth'] as String,
    json['fullName'] as String,
    json['jobTitle'] as String,
    (json['totalDay'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$ReportDetailModelToJson(ReportDetailModel instance) =>
    <String, dynamic>{
      'fullName': instance.fullName,
      'totalDay': instance.totalDay,
      'dateOfBirth': instance.dateOfBirth,
      'jobTitle': instance.jobTitle,
    };
