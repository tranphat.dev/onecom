// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'json_object.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonObject _$JsonObjectFromJson(Map<String, dynamic> json) {
  return JsonObject(
    json['key'] as String,
    json['value'] as String,
  );
}

Map<String, dynamic> _$JsonObjectToJson(JsonObject instance) =>
    <String, dynamic>{
      'key': instance.key,
      'value': instance.value,
    };

abstract class _$JsonObjectSerializerMixin {
  String get key;
  String get value;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'key': key, 'value': value};
}

