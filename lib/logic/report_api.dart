import 'package:dio/dio.dart';
import '../all_file.dart';

class ReportApi {
  Dio dio = new Dio(BaseOptions(
    baseUrl: AppConfig.SERVER_URL,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  ));

  Future<List<ReportDetailModel>> getShiftReport({
    String team_id,
    DateTime from,
    DateTime to,
  }) async {
    final apiUrl = '/report';
    final FormData data = new FormData.fromMap({
      'team_id': team_id,
      'from': from,
      'to': to,
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      appState.log = '';
      List<ReportDetailModel> result = response.data['report']
          .map((i) => ReportDetailModel.fromJson(i))
          .cast<ReportDetailModel>()
          .toList();

      return result;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
