import 'dart:convert';

import 'package:dio/dio.dart';
import '../all_file.dart';

class NewsApi {
  Dio dio = new Dio(BaseOptions(
    baseUrl: AppConfig.SERVER_URL,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  ));

  createNews(
      {String url,
      String title,
      String description,
      File imgUrl,
      String createBy,
      String team_id}) async {
    final apiUrl = '/create_news';
    String image64;
    if (imgUrl != null) {
      List<int> imageBytes = await imgUrl.readAsBytes();
      image64 = base64Encode(imageBytes);
    }
    final FormData data = new FormData.fromMap({
      'url': url,
      'title': title,
      'description': description,
      'imgUrl': image64,
      'createBy': createBy,
      'team_id': team_id
    });
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  updateNews({
    String id,
    String url,
    String title,
    String description,
    File imgUrl,
  }) async {
    final apiUrl = '/update_news/$id';
    FormData data;
    String image64;
    if (imgUrl != null) {
      List<int> imageBytes = await imgUrl.readAsBytes();
      image64 = base64Encode(imageBytes);
      data = new FormData.fromMap({
        'id': id,
        'url': url,
        'title': title,
        'description': description,
        'imgUrl': image64,
      });
    } else {
      data = new FormData.fromMap({
        'id': id,
        'url': url,
        'title': title,
        'description': description,
      });
    }
    try {
      Response response = await dio.post(apiUrl,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  deleteNews(String id) async {
    final url = '/delete_news/$id';
    final FormData data = new FormData.fromMap({
      'id': id,
    });
    try {
      Response response = await dio.post(url,
          data: data,
          options:
              Options(headers: {'Authorization': 'Bearer ${appState.token}'}));
      if (response.data['success']) {
        appState.log = '';
        return true;
      } else {
        appState.log = response.data['message'];
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }
}
