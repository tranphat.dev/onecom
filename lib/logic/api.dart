import 'package:one_com/logic/news_api.dart';
import 'package:one_com/logic/shiftwork_api.dart';
import 'package:one_com/logic/team_api.dart';
import 'package:one_com/logic/user_api.dart';

import 'report_api.dart';

class Api with TeamApi, UserApi, NewsApi, ShiftWork, ReportApi {}

final Api api = Api();
