class VinaDateUtils {
  static const List<String> MONTH_FULL_TH = [
    "Tháng 1",
    "Tháng 2",
    "Tháng 3",
    "Tháng 4",
    "Tháng 5",
    "Tháng 6",
    "Tháng 7",
    "Tháng 8",
    "Tháng 9",
    "Tháng 10",
    "Tháng 11",
    "Tháng 12",
  ];

  static const List<String> MONTH_SHORT_TH = [
    "Tháng 1",
    "Tháng 2",
    "Tháng 3",
    "Tháng 4",
    "Tháng 5",
    "Tháng 6",
    "Tháng 7",
    "Tháng 8",
    "Tháng 9",
    "Tháng 10",
    "Tháng 11",
    "Tháng 12",
  ];

  static getMonthNameShot(int month) {
    return MONTH_SHORT_TH[month - 1];
  }

  static getMonthNameFull(int month) {
    return MONTH_FULL_TH[month - 1];
  }
}
