import '../all_file.dart';

class CreateTeam {
  TextEditingController popupValue = TextEditingController();

  Widget _buildPopupCreate(TeamModel model, BuildContext context, scaffoldKey) {
    teamBloc.teamNameController.text = model?.teamName ?? '';
    teamBloc.teamCodeController.text = model?.teamCode ?? '';
    teamBloc.teamEmailController.text = model?.email ?? '';
    teamBloc.teamPhoneController.text = model?.phone ?? '';
    teamBloc.teamAddressController.text = model?.address ?? '';
    teamBloc.timeLimitController.text = model?.timeLimit ?? '';
    teamBloc.ipWifiController.text = model?.ipWifi ?? '';

    String label = (model == null) ? 'LBL_CREATE_TEAM' : 'LBL_EDIT_TEAM';
    String getLabel = Languages.get(label);
    String delete = Languages.get('LBL_DELETE');

    ProgressDialog pr;
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: Languages.get('LBL_PLEASE_WAIT'));
    return StreamBuilder(
        stream: reloadBloc.reloadStream,
        builder: (context, snapshot) {
          return Container(
            child: Column(
              children: <Widget>[
                CustomTextField(
                  label: 'LBL_TEAM_NAME',
                  hint: 'LBL_TEAM_NAME_HINT',
                  controller: teamBloc.teamNameController,
                  stream: teamBloc.teamNameStream,
                  shortField: false,
                  required: true,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CustomTextField(
                      label: 'LBL_TEAM_CODE',
                      hint: 'LBL_TEAM_CODE_HINT',
                      controller: teamBloc.teamCodeController,
                      stream: teamBloc.teamCodeStream,
                      required: true,
                    ),
                    CustomTextField(
                      label: 'LBL_TEAM_EMAIL',
                      hint: 'LBL_TEAM_EMAIL_HINT',
                      controller: teamBloc.teamEmailController,
                      stream: teamBloc.teamEmailStream,
                      required: true,
                    ),
                  ],
                ),
                CustomTextField(
                  label: 'LBL_TEAM_ADDRESS',
                  hint: 'LBL_TEAM_ADDRESS_HINT',
                  controller: teamBloc.teamAddressController,
                  shortField: false,
                ),
//                CustomTextField(
//                  label: 'LBL_TEAM_TIMELIMIT',
//                  hint: 'LBL_TEAM_TIMELIMIT_HINT',
//                  controller: teamBloc.timeLimitController,
//                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CustomTextField(
                      label: 'LBL_TEAM_PHONE',
                      hint: 'LBL_TEAM_PHONE_HINT',
                      controller: teamBloc.teamPhoneController,
                    ),
                    CustomTextField(
                      label: 'LBL_TEAM_IPWIFI',
                      hint: 'LBL_TEAM_IPWIFI_HINT',
                      controller: teamBloc.ipWifiController,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CustomOverlayWidget(
                      onTap: () async {
                        if (model == null) {
                          if (teamBloc.validate()) {
                            bool result = await teamBloc.create();
                            if (result) {
                              api.reload().then((value) {
                                Navigator.of(context).pop();
                              });
                              myTheme.showSuccessNoti(scaffoldKey, getLabel);
                            } else {
                              myTheme.showErrorNoti(null, getLabel);
                            }
                          }
                        } else {
                          if (teamBloc.validateEdit()) {
                            pr.show();
                            bool result = await teamBloc.update(model.id);
                            if (result) {
                              Future.delayed(Duration(seconds: 0))
                                  .then((value) {
                                pr.hide().whenComplete(() {
                                  api.reload().then((value) {
                                    Navigator.of(context).pop();
                                  });
                                });
                              });
                              myTheme.showSuccessNoti(scaffoldKey, getLabel);
                            } else {
                              myTheme.showErrorNoti(null, getLabel);
                            }
                          }
                        }
                      },
                      borderRadius: 20,
                      margin: EdgeInsets.only(top: 30),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      backgroundColor: myTheme.greenPrimary,
                      child: Center(
                          child: Text(
                        getLabel,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      )),
                    ),
//                    (model == null)
//                        ? Container()
//                        : CustomOverlayWidget(
//                      onTap: () async {
//                        bool result = await teamBloc.delete(model.id);
//                        if (result) {
//                          myTheme.showSuccessNoti(
//                              scaffoldKey, '$delete ');
//                          Navigator.of(context).pop();
//                          reloadBloc.reload();
//                        } else {
//                          myTheme.showErrorNoti(null, '$delete ');
//                        }
//                      },
//                      isDelete: true,
//                      borderRadius: 20,
//                      margin: EdgeInsets.only(top: 30, left: 10),
//                      padding: EdgeInsets.symmetric(
//                          horizontal: 20, vertical: 10),
//                      backgroundColor: Colors.red,
//                      child: Center(
//                          child: Text(
//                            delete,
//                            style: TextStyle(color: Colors.white, fontSize: 20),
//                          )),
//                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }

  showEditTeamPopup(BuildContext context, {TeamModel model, scaffoldKey}) {
    myTheme.openAddEntryDialog(context,
        label: Languages.get('LBL_TEAM'),
        title: Container(),
        body: _buildPopupCreate(model, context, scaffoldKey));
  }
}
