import '../all_file.dart';

class CreateUser {
  Widget _buildPopupCreate(UserModel model, BuildContext context, scaffoldKey) {
    userBloc.firstNameController.text = model?.firstName ?? '';
    userBloc.lastNameController.text = model?.lastName ?? '';
    userBloc.dobController =
        (model?.dateOfBirth != '' && model?.dateOfBirth != null)
            ? DateTime.parse(model?.dateOfBirth)
            : null;
    dateBloc.setSelectedDate(userBloc?.dobController ?? null);
    userBloc.phoneController.text = model?.phone ?? '';
    userBloc.emailController.text = model?.email ?? '';
    userBloc.jobTitleController.text = model?.jobTitle ?? '';
    userBloc.usernameController.text = model?.username ?? '';
    userBloc.passwordController.text = model?.password ?? '';

    String label = (model == null) ? 'LBL_CREATE_USER' : 'LBL_EDIT_USER';
    String getLabel = Languages.get(label);
    String delete = Languages.get('LBL_DELETE');

    ProgressDialog pr;
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: Languages.get('LBL_PLEASE_WAIT'));

    return StreamBuilder(
      stream: null,
      builder: (context, snapshot) {
        return SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CustomTextField(
                      label: 'LBL_FIRST_NAME',
                      hint: 'LBL_FIRST_NAME_HINT',
                      controller: userBloc.firstNameController,
                      stream: userBloc.firstNameStream,
                    ),
                    CustomTextField(
                      label: 'LBL_LAST_NAME',
                      hint: 'LBL_LAST_NAME_HINT',
                      controller: userBloc.lastNameController,
                      stream: userBloc.lastNameStream,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CustomTextField(
                      label: 'LBL_PHONE',
                      hint: 'LBL_PHONE_HINT',
                      controller: userBloc.phoneController,
                      stream: userBloc.phoneStream,
                      keyboardType: TextInputType.phone,
                    ),
                    CustomTextField(
                      label: 'LBL_DOB',
                      required: true,
                      customTextFieldType: CustomTextFieldType.DATE_PICKER,
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                CustomTextField(
                  label: 'LBL_EMAIL',
                  hint: 'LBL_EMAIL_HINT',
                  controller: userBloc.emailController,
                  stream: userBloc.emailStream,
                  shortField: false,
                ),
                SizedBox(
                  height: 30,
                ),
                (model == null)
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          CustomTextField(
                            label: 'LBL_USERNAME',
                            hint: 'LBL_USERNAME_HINT',
                            controller: userBloc.usernameController,
                            stream: userBloc.usernameStream,
                          ),
                          CustomTextField(
                            label: 'LBL_PASSWORD',
                            hint: 'LBL_PASSWORD_HINT',
                            controller: userBloc.passwordController,
                            stream: userBloc.passwordStream,
                          ),
                        ],
                      )
                    : Container(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CustomOverlayWidget(
                      onTap: () async {
                        if (model == null) {
                          if (userBloc.validate()) {
                            pr.show();
                            bool result = await userBloc.create();
                            if (result) {
                              Future.delayed(Duration(seconds: 0))
                                  .then((value) {
                                pr.hide().whenComplete(() {
                                  api.reload().then((value) {
                                    Navigator.of(context).pop();
                                    fileBloc.image = null;
                                  });
                                });
                              });
                              myTheme.showSuccessNoti(scaffoldKey, getLabel);
                            } else {
                              Future.delayed(Duration(seconds: 0))
                                  .then((value) {
                                pr.hide().whenComplete(() {
                                  myTheme.showErrorNoti(null, getLabel);
                                });
                              });
                            }
                          }
                        } else {
                          if (userBloc.validateEdit()) {
                            pr.show();
                            bool result = await userBloc.update(model.id);
                            if (result) {
                              Future.delayed(Duration(seconds: 0))
                                  .then((value) {
                                pr.hide().whenComplete(() {
                                  api.reload().then((value) {
                                    Navigator.of(context).pop();
                                    fileBloc.image = null;
                                  });
                                });
                              });
                              myTheme.showSuccessNoti(scaffoldKey, getLabel);
                            } else {
                              Future.delayed(Duration(seconds: 0))
                                  .then((value) {
                                pr.hide().whenComplete(() {
                                  myTheme.showErrorNoti(null, getLabel);
                                });
                              });
                            }
                          }
                        }
                      },
                      borderRadius: 20,
                      margin: EdgeInsets.only(top: 30),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      backgroundColor: myTheme.greenPrimary,
                      child: Center(
                        child: Text(
                          getLabel,
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ),
                    (model == null)
                        ? Container()
                        : (model.id != appState.currentUser.id)
                            ? CustomOverlayWidget(
                                onTap: () async {
                                  bool result = await userBloc.delete(model.id);
                                  if (result) {
                                    myTheme.showSuccessNoti(
                                        scaffoldKey, '$delete ');
                                    Navigator.of(context).pop();
                                    reloadBloc.reload();
                                  } else {
                                    myTheme.showErrorNoti(null, '$delete ');
                                  }
                                },
                                isDelete: true,
                                borderRadius: 20,
                                margin: EdgeInsets.only(top: 30, left: 10),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                backgroundColor: Colors.red,
                                child: Center(
                                    child: Text(
                                  delete,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                )),
                              )
                            : Container(),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildTitlePopup(UserModel model, BuildContext context, scaffoldKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        (model == null)
            ? myTheme.imagePicker(myTheme.greenPrimary, 'ic_user', 80)
            : CustomAvatar(
                size: 80,
                defaultSvg: 'ic_user',
                avatarUrl: model.avataUrl,
                picker: true,
              ),
        Container(),
        CustomTextField(
          label: 'LBL_JOB_TITLE',
          hint: 'LBL_JOB_TITLE_HINT',
          controller: userBloc.jobTitleController,
          stream: userBloc.jobTitleStream,
        ),
      ],
    );
  }

  showEditUserPopup(BuildContext context, {UserModel model, scaffoldKey}) {
    myTheme.openAddEntryDialog(context,
        label: Languages.get('LBL_USER'),
        title: _buildTitlePopup(model, context, scaffoldKey),
        body: _buildPopupCreate(model, context, scaffoldKey));
  }
}
