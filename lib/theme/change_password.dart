import 'package:one_com/bloc/change_pass_bloc.dart';
import 'package:one_com/widget/material_button.dart';

import '../all_file.dart';

class ChangePassword {
  Widget _buildPopupCreate(context) {
    return Container(
      constraints: BoxConstraints.expand(),
      padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
      child: StreamBuilder(
          stream: changePassBloc.updateStream,
          builder: (context, snapshot) {
            return SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 30, 0, 0),
                        child: Text(
                          Languages.get('LBL_CURRENT_PASS'),
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: myTheme.greenPrimary),
                        ),
                      ),
                      (changePassBloc.currentError)
                          ? Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                Languages.get('LBL_CURRENT_PASS_ERROR'),
                                style: TextStyle(color: Colors.red),
                              ),
                            )
                          : SizedBox(height: 10),
                      CustomOverlayWidget(
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextFormField(
                          focusNode: changePassBloc.nodeOne,
                          onFieldSubmitted: (term) {
                            changePassBloc.nodeOne.unfocus();
                            FocusScope.of(context)
                                .requestFocus(changePassBloc.nodeTwo);
                          },
                          obscureText: true,
                          controller: changePassBloc.currentController,
                          textInputAction: TextInputAction.next,
                          style: TextStyle(fontSize: 16),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 20),
                            hintText: Languages.get('LBL_CURRENT_PASS_HINT'),
                            hintStyle: TextStyle(
                              fontSize: 16,
                              color: (changePassBloc.currentError)
                                  ? Colors.red
                                  : myTheme.gray,
                            ),
                            fillColor: Colors.white,
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: myTheme.greenPrimary, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: (changePassBloc.currentError)
                                      ? Colors.red
                                      : Colors.black,
                                  width: 0.2),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                        child: Text(
                          Languages.get('LBL_NEW_PASS'),
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: myTheme.greenPrimary),
                        ),
                      ),
                      (changePassBloc.newError)
                          ? Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                Languages.get('LBL_NEW_PASS_ERROR'),
                                style: TextStyle(color: Colors.red),
                              ),
                            )
                          : (changePassBloc.compareError)
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    Languages.get('LBL_COMPARE_PASS_ERROR'),
                                    style: TextStyle(color: Colors.red),
                                  ),
                                )
                              : SizedBox(height: 10),
                      CustomOverlayWidget(
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextFormField(
                          focusNode: changePassBloc.nodeTwo,
                          onFieldSubmitted: (term) {
                            changePassBloc.nodeTwo.unfocus();
                            FocusScope.of(context)
                                .requestFocus(changePassBloc.nodeThree);
                          },
                          textInputAction: TextInputAction.next,
                          obscureText: true,
                          controller: changePassBloc.newController,
                          style: TextStyle(fontSize: 16),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 20),
                            hintText: Languages.get('LBL_NEW_PASS_HINT'),
                            hintStyle: TextStyle(
                              fontSize: 16,
                              color: (changePassBloc.newError)
                                  ? Colors.red
                                  : myTheme.gray,
                            ),
                            fillColor: Colors.white,
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: myTheme.greenPrimary, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: (changePassBloc.newError)
                                      ? Colors.red
                                      : Colors.black,
                                  width: 0.2),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                        child: Text(
                          Languages.get('LBL_CONFIRM_PASS'),
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: myTheme.greenPrimary),
                        ),
                      ),
                      (changePassBloc.confirmError)
                          ? Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                Languages.get('LBL_CONFIRM_PASS_ERROR'),
                                style: TextStyle(color: Colors.red),
                              ),
                            )
                          : SizedBox(height: 10),
                      CustomOverlayWidget(
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: TextFormField(
                          focusNode: changePassBloc.nodeThree,
                          onFieldSubmitted: (term) {
                            changePassBloc.nodeThree.unfocus();
                          },
                          textInputAction: TextInputAction.done,
                          obscureText: true,
                          controller: changePassBloc.confirmController,
                          style: TextStyle(fontSize: 16),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 20),
                            hintText: Languages.get('LBL_CONFIRM_PASS_HINT'),
                            hintStyle: TextStyle(
                              fontSize: 16,
                              color: (changePassBloc.confirmError)
                                  ? Colors.red
                                  : myTheme.gray,
                            ),
                            fillColor: Colors.white,
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: myTheme.greenPrimary, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: (changePassBloc.confirmError)
                                      ? Colors.red
                                      : Colors.black,
                                  width: 0.2),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            MyMaterialButton(
                              label: 'LBL_CANCEL',
                              width: getPercentWidth(context, 0.3, null),
                              borderRadius: 100,
                              height: 45,
                              colors: Colors.redAccent,
                              callback: () => _cancel(context),
                            ),
                            MyMaterialButton(
                              label: 'LBL_SAVE',
                              width: getPercentWidth(context, 0.3, null),
                              borderRadius: 100,
                              height: 45,
                              colors: myTheme.greenSecondary,
                              callback: () => _save(context),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }

  void _save(context) async {
    final prefs = await SharedPreferences.getInstance();
    String password = prefs.getString('password');
    if (changePassBloc.currentController.text != password) {
      changePassBloc.currentError = true;
      changePassBloc.reload();
    } else {
      changePassBloc.currentError = false;
      changePassBloc.reload();
    }

    if (changePassBloc.newController.text.length < 6) {
      changePassBloc.newError = true;
      changePassBloc.reload();
    } else {
      changePassBloc.newError = false;
      changePassBloc.reload();
    }

    if (changePassBloc.newController.text !=
        changePassBloc.confirmController.text) {
      changePassBloc.confirmError = true;
      changePassBloc.reload();
    } else {
      changePassBloc.confirmError = false;
      changePassBloc.reload();
    }

    if (!changePassBloc.confirmError &&
        !changePassBloc.newError &&
        !changePassBloc.currentError) {
      bool result = await api.changePassword(
          old_password: changePassBloc.currentController.text,
          new_password: changePassBloc.newController.text);
      if (result) {
        prefs.setString('password', changePassBloc.newController.text);
        Navigator.pop(context);
        changePassBloc.clear();
        //TODO: SUCCESS
      } else {
        changePassBloc.compareError = true;
        changePassBloc.reload();
        //TODO : FAIL
      }
    }
  }

  void _cancel(context) {
    changePassBloc.clear();
    Navigator.pop(context);
  }

  showChangePasswordPopup(BuildContext context) {
    myTheme.openAddEntryDialog(
      context,
      label: Languages.get('LBL_CHANGE_PASSWORD'),
      title: Container(),
      body: _buildPopupCreate(context),
    );
  }
}
