import '../../../all_file.dart';

class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  List<UserModel> users = List<UserModel>();
  TextEditingController searchController = TextEditingController();

  final styleText = TextStyle(
    color: myTheme.black,
    fontSize: 13,
  );

  @override
  void initState() {
    super.initState();
    users.addAll(appState.listUser);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return KeyboardDismisser(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: myTheme.greenPrimary,
          title: Text(
            Languages.get('LBL_DIRECTORY'),
            style: TextStyle(color: myTheme.white),
          ),
        ),
        backgroundColor: myTheme.backgroundColor,
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildScanAndSearch(),
//              _buildTitle('LBL_YOUR_CARD'),
//              _buildContactYourself(),
              SizedBox(
                height: 10,
              ),
//              _buildTitle('LBL_CONTACTS'),
              Expanded(
                child: StreamBuilder(
                  stream: reloadBloc.reloadStream,
                  builder: (context, snapshot) {
                    filterSearchResults(searchController.text);
                    if (users.length == 0)
                      return Center(
                        child: Text(Languages.get('LBL_NO_DATA_AVAILABLE')),
                      );
                    return ListView.builder(
                      itemCount: users.length,
                      itemBuilder: (context, index) => _buildListContact(index),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildContactYourself() {
    UserModel model = appState.currentUser;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15),
      decoration: myTheme.defaultDecoration,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: ListTile(
          title: Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Row(
              children: <Widget>[
                Text(
                  model.firstName + ' ' + model.lastName,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: myTheme.greenSecondary,
                  ),
                ),
              ],
            ),
          ),
          subtitle: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.phone,
                      color: myTheme.black,
                      size: 15,
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      model?.phone ?? Languages.get('LBL_NONE'),
                      style: styleText,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListContact(int index) {
    UserModel model = users[index];
    if (model.id == appState.currentUser.id) return Container();
    return Column(
      children: <Widget>[
        ListTile(
          leading: model.getAvatar(60),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                (model.firstName != null)
                    ? model.firstName + ' ' + model.lastName
                    : '',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: myTheme.greenSecondary,
                ),
              ),
            ],
          ),
          subtitle: Row(
            children: <Widget>[
              Icon(
                Icons.phone,
                color: myTheme.black,
                size: 15,
              ),
              SizedBox(
                width: 15,
              ),
              Text(
                model?.phone ?? Languages.get('LBL_NONE'),
                style: styleText,
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30),
          child: Divider(
            color: myTheme.black,
          ),
        ),
      ],
    );
  }

  Widget _buildTitle(label) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 20,
      ),
      child: Text(
        Languages.get(label),
        style: TextStyle(
          color: myTheme.greenPrimary,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _buildScanAndSearch() {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 10, left: 10, right: 10),
      color: myTheme.greenPrimary,
      child: CustomOverlayWidget(
        borderRadius: 10,
        height: 50,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: TextField(
          onChanged: (value) {
            reloadBloc.reload();
          },
          controller: searchController,
          decoration: InputDecoration.collapsed(
            fillColor: Colors.white,
            hintText: Languages.get('LBL_SEARCH'),
            filled: true,
          ),
        ),
      ),
    );
  }

  void filterSearchResults(String query) {
    List<UserModel> dummySearchList = List<UserModel>();
    dummySearchList.addAll(appState.listUser);
    if (query.isNotEmpty) {
      List<UserModel> dummyListData = List<UserModel>();
      dummySearchList.forEach((item) {
        if (item.firstName.toUpperCase().contains(query.toUpperCase()) ||
            item.lastName.toUpperCase().contains(query.toUpperCase())) {
          dummyListData.add(item);
        }
      });

      users.clear();
      users.addAll(dummyListData);

      return;
    } else {
      users.clear();
      users.addAll(appState.listUser);
    }
  }
}
