import '../../../all_file.dart';

class TeamInfo extends StatefulWidget {
  @override
  _TeamInfoState createState() => _TeamInfoState();
}

class _TeamInfoState extends State<TeamInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myTheme.backgroundColor,
      appBar: AppBar(
        backgroundColor: myTheme.greenPrimary,
        title: Text(
          Languages.get('LBL_TEAM'),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.0,
            color: Colors.white,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(myTheme.borderRadius),
          ),
        ),
      ),
      body: _buildInfoTeam(),
    );
  }

  Widget _buildInfoTeam() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            padding: EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _buildDetailInfo(Icons.account_balance, 'LBL_TEAM_NAME',
                    appState.teamInfo.teamName),
                _buildDetailInfo(
                    Icons.book, 'LBL_TEAM_CODE', appState.teamInfo.teamCode),
                _buildDetailInfo(
                    Icons.email, 'LBL_EMAIL', appState.teamInfo.email),
                _buildDetailInfo(
                    Icons.phone, 'LBL_PHONE', appState.teamInfo.phone),
                _buildDetailInfo(Icons.location_on, 'LBL_TEAM_ADDRESS',
                    appState.teamInfo.address),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDetailInfo(IconData icon, label, content) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Icon(
                      icon,
                      size: 20,
                      color: myTheme.black,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      Languages.get(label),
                      style: TextStyle(
                        fontSize: 16,
                        color: myTheme.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Text(
                  (content != null) ? content : '',
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 16,
                    color: myTheme.greenSecondary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          Divider(),
        ],
      ),
    );
  }
}
