import 'package:one_com/widget/calendar.dart';

import '../../../all_file.dart';

class Schedule extends StatefulWidget {
  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<Schedule> {
  bool inCompany = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    getPublicIP().then((ip) {
      if (ip == ip) {
        setState(() {
          inCompany = true;
        });
      } else {
        setState(() {
          inCompany = false;
        });
      }
    });
    return Scaffold(
      key: _scaffoldKey,
      body: CustomScrollView(
        slivers: <Widget>[
          HomeHeaderExpanded(
            title: Languages.get('LBL_CALENDAR_FULL'),
            actions: [
              Container(
                width: 56.0,
                child: RawMaterialButton(
                  padding: EdgeInsets.all(0.0),
                  shape: CircleBorder(),
                  onPressed: () {
                    _showDialogInfoShift();
                  },
                  child: Icon(
                    Icons.book,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Calendar(),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _tutorialItem(Languages.get('LBL_REGIS_ALL_DAY'),
                            Colors.blueAccent),
                        _tutorialItem(Languages.get('LBL_REGIS_HALF_DAY'),
                            myTheme.greenSecondary),
                        _tutorialItem(
                            Languages.get('LBL_ATTENDED'), myTheme.gray),
                        _tutorialItem(Languages.get('LBL_REGIS_NOT_ATTEND'),
                            Colors.redAccent),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    child: (checkinBloc.getModel() != null)
                        ? _buildCheckinButton()
                        : Container(),
                  ),
                ],
              ),
            ]),
          )
        ],
      ),
    );
  }

  Widget _buildCheckinButton() {
    ProgressDialog pr;
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: Languages.get('LBL_PLEASE_WAIT'));
    return MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(40),
      ),
      color: checkinBloc.colorButton(),
      child: Text(
        checkinBloc.contentButton(),
        style: TextStyle(color: myTheme.white),
      ),
      onPressed: () {
        if (appState.regisToday.isCheckout == 1) {
          myTheme.showMadeNoti(_scaffoldKey, Languages.get('LBL_CHECKED'));
        } else {
          pr.show();
          getPublicIP().then((ip) {
            if (ip == appState.teamInfo.ipWifi) {
              Future.delayed(Duration(seconds: 0)).then((value) {
                pr.hide().whenComplete(() {
                  checkinBloc.checkin();
                });
              });
            } else {
              Future.delayed(Duration(seconds: 0)).then((value) {
                pr.hide().whenComplete(() {
                  myTheme.showErrorNotiWithoutSub(
                      _scaffoldKey, Languages.get('LBL_IP_NOT_MATCH'));
                });
              });
            }
          });
        }
      },
    );
  }

  Widget _tutorialItem(String tutContent, Color color) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(3),
          height: 18,
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(2)),
              color: color),
          child: Container(
            width: 50,
            padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
            child: Center(
              child: Text(Languages.get('LBL_SHIFT'),
                  style:
                      TextStyle().copyWith(color: Colors.white, fontSize: 11),
                  overflow: TextOverflow.ellipsis),
            ),
          ),
        ),
        Text(
          tutContent,
          style: TextStyle(color: myTheme.black, fontSize: 11),
        ),
      ],
    );
  }

  void _showDialogInfoShift() {
    showDialog(
      context: context,
      child: SimpleDialog(
        title: Row(
          children: <Widget>[
            Text(
              Languages.get('LBL_INFOR_LIST_SHIFT'),
              style: TextStyle(
                fontSize: 18,
                color: myTheme.greenPrimary,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            child: ListView.builder(
              itemCount: appState.listShiftwork.length,
              itemExtent: 50,
              itemBuilder: (context, index) => _shiftItem(index),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  color: myTheme.greenPrimary,
                  child: Text(
                    Languages.get('LBL_OK'),
                    style: TextStyle(color: myTheme.white),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _shiftItem(int index) {
    ConfigModel model = appState.listShiftwork[index];
    TimeOfDay begin = getTimeOfDay(model.begin);
    TimeOfDay end = getTimeOfDay(model.end);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(3),
          height: 20,
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.all(Radius.circular(2)),
            color: (model.shiftworkName == 'All Day')
                ? Colors.blueAccent
                : myTheme.greenSecondary,
          ),
          child: Container(
            width: 100,
            padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
            child: Center(
              child: Text(model.shiftworkName,
                  style:
                      TextStyle().copyWith(color: Colors.white, fontSize: 12),
                  overflow: TextOverflow.ellipsis),
            ),
          ),
        ),
        Row(
          children: <Widget>[
            Text(
              getStringTimeOfDay(begin) + ' - ',
              style: TextStyle(color: myTheme.black, fontSize: 12),
            ),
            Text(
              getStringTimeOfDay(end),
              style: TextStyle(color: myTheme.black, fontSize: 12),
            ),
          ],
        ),
      ],
    );
  }
}
