import '../../../../all_file.dart';

class NewsManagement extends StatefulWidget {
  @override
  _NewsManagementState createState() => _NewsManagementState();
}

class _NewsManagementState extends State<NewsManagement> {
  final SlidableController slideAbleController = SlidableController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: myTheme.backgroundColor,
      appBar: AppBar(
        backgroundColor: myTheme.greenPrimary,
        title: Text(
          Languages.get('LBL_NEWS_MANAGE'),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.0,
            color: Colors.white,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(myTheme.borderRadius),
          ),
        ),
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => slideAbleController.activeState?.close(),
        child: StreamBuilder(
          stream: reloadBloc.reloadStream,
          builder: (context, snapshot) {
            return Container(
              margin: EdgeInsets.only(top: 20),
              constraints: BoxConstraints.expand(),
              color: myTheme.backgroundColor,
              child: ListView.builder(
                itemCount: appState.listNews.length,
                itemBuilder: (context, index) => _buildNews(index),
              ),
            );
          }
        ),
      ),
    );
  }

  Widget _buildNews(int index) {
    NewsModel model = appState.listNews[index];
    return Slidable(
      controller: slideAbleController,
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      closeOnScroll: true,
      child: GestureDetector(
        onTap: () {
          setState(() {
            myTheme.showEditNewsPopup(context,
                model: model, scaffoldKey: _scaffoldKey);
          });
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15),
          decoration: myTheme.defaultDecoration,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: ListTile(
              leading: CustomAvatar(
                size: 50,
                defaultSvg: 'ic_news',
                avatarUrl: model.imgUrl,
                picker: true,
              ),
              title: Padding(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: Text(
                  model.title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: myTheme.greenSecondary,
                  ),
                ),
              ),
              subtitle: Text(
                model.description,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
              isThreeLine: true,
            ),
          ),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          iconWidget: Icon(
            Icons.edit,
            color: myTheme.greenPrimary,
            size: 30,
          ),
          color: myTheme.backgroundColor,
          foregroundColor: myTheme.black,
          onTap: () => myTheme.showEditNewsPopup(context,
              model: model, scaffoldKey: _scaffoldKey),
        ),
//        IconSlideAction(
//          iconWidget: Icon(
//            Icons.delete,
//            color: Colors.red,
//            size: 30,
//          ),
//          color: myTheme.backgroundColor,
//          foregroundColor: myTheme.black,
//          onTap: () {},
//        ),
      ],
    );
  }
}
