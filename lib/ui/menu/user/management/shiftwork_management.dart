import '../../../../all_file.dart';

class ShiftworkManagement extends StatefulWidget {
  @override
  _ShiftworkManagementState createState() => _ShiftworkManagementState();
}

class _ShiftworkManagementState extends State<ShiftworkManagement> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: myTheme.backgroundColor,
      appBar: AppBar(
        backgroundColor: myTheme.greenPrimary,
        title: Text(
          Languages.get('LBL_SHIFTWORK_MANAGE'),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.0,
            color: Colors.white,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(myTheme.borderRadius),
          ),
        ),
      ),
      body: StreamBuilder(
          stream: reloadBloc.reloadStream,
          builder: (context, snapshot) {
            return Container(
              margin: EdgeInsets.only(top: 10),
              child: ListView.builder(
                itemCount: appState.listShiftwork.length,
                itemBuilder: (context, index) => _buildShiftwork(index),
              ),
            );
          }),
    );
  }

  Widget _buildShiftwork(int index) {
    ConfigModel model = appState.listShiftwork[index];
    String beginTime = model?.begin ?? '00:00:00';
    TimeOfDay _startTime = TimeOfDay(
        hour: int.parse(beginTime.split(":")[0]),
        minute: int.parse(beginTime.split(":")[1]));

    String endTime = model?.end ?? '00:00:00';
    TimeOfDay _endTime = TimeOfDay(
        hour: int.parse(endTime.split(":")[0]),
        minute: int.parse(endTime.split(":")[1]));
    final styleText = TextStyle(
      color: myTheme.black,
      fontSize: 13,
    );
    return GestureDetector(
      onTap: () {
        timeBloc.clear();
        myTheme.showEditConfigPopup(context,
            model: model, scaffoldKey: _scaffoldKey);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15),
        decoration: myTheme.defaultDecoration,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          child: ListTile(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Text(
                (model.shiftworkName != null)
                    ? model.shiftworkName
                    : Languages.get('LBL_NONE'),
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: myTheme.greenSecondary,
                ),
              ),
            ),
            subtitle: Row(
              children: <Widget>[
                Text(
                  getStringTimeOfDay(_startTime) + ' - ',
                  style: styleText,
                ),
                Text(
                  getStringTimeOfDay(_endTime),
                  style: styleText,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
