import 'package:one_com/model/team/team_model.dart';

import '../../../../all_file.dart';

class TeamManagement extends StatefulWidget {
  @override
  _TeamManagementState createState() => _TeamManagementState();
}

class _TeamManagementState extends State<TeamManagement> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: myTheme.backgroundColor,
      appBar: AppBar(
        backgroundColor: myTheme.greenPrimary,
        title: Text(
          Languages.get('LBL_TEAM_MANAGE'),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            letterSpacing: 1.0,
            color: Colors.white,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(myTheme.borderRadius),
          ),
        ),
      ),
      body: StreamBuilder(
        stream: reloadBloc.reloadStream,
        builder: (context, snapshot) {
          return _buildInfoTeam();
        }
      ),
    );
  }

  Widget _buildInfoTeam() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10,
            ),
            padding: EdgeInsets.all(10.0),
//            decoration: myTheme.defaultDecoration,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _buildDetailInfo(Icons.account_balance, 'LBL_TEAM_NAME',
                    appState.teamInfo.teamName),
                _buildDetailInfo(
                    Icons.book, 'LBL_TEAM_CODE', appState.teamInfo.teamCode),
                _buildDetailInfo(
                    Icons.email, 'LBL_EMAIL', appState.teamInfo.email),
                _buildDetailInfo(
                    Icons.phone, 'LBL_PHONE', appState.teamInfo.phone),
                _buildDetailInfo(Icons.location_on, 'LBL_TEAM_ADDRESS',
                    appState.teamInfo.address),
//                _buildDetailInfo(
//                    Icons.timer, 'LBL_TEAM_TIMELIMIT', appState.teamInfo.timeLimit),
                _buildDetailInfo(
                    Icons.timer, 'LBL_TEAM_IPWIFI', appState.teamInfo.ipWifi),
              ],
            ),
          ),
          SizedBox(
            height: 50,
          ),
          _buildEditButton(),
        ],
      ),
    );
  }

  Widget _buildDetailInfo(IconData icon, label, content) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Icon(
                      icon,
                      size: 20,
                      color: myTheme.black,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      Languages.get(label),
                      style: TextStyle(
                        fontSize: 16,
                        color: myTheme.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Text(
                  (content != null) ? content : '',
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 16,
                    color: myTheme.greenSecondary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          Divider(),
        ],
      ),
    );
  }

  Widget _buildEditButton() {
    TeamModel model = appState.teamInfo;
    return GestureDetector(
      onTap: () => myTheme.showEditTeamPopup(context,
          model: model, scaffoldKey: _scaffoldKey),
      child: Container(
        width: 100,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: myTheme.greenSecondary,
          boxShadow: [
            BoxShadow(
              color: myTheme.greenSecondary,
              blurRadius: 2.0,
              spreadRadius: 0.5,
              offset: Offset(
                0,
                2.0,
              ),
            ),
          ],
        ),
        child: Center(
          child: Text(
            Languages.get('LBL_EDIT'),
            style: TextStyle(
              color: myTheme.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
