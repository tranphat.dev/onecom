import '../all_file.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  ProgressDialog pr;

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
            Text(Languages.get('LBL_BACK'),
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  Widget _submitButton() {
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: Languages.get('LBL_PLEASE_WAIT'));

    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () async {
            if (teamBloc.validate()) {
              pr.show();
              bool result = await teamBloc.create();
              if (result) {
                Future.delayed(Duration(seconds: 1)).then((value) {
                  pr.hide().whenComplete(() {
                    Navigator.of(context).pushNamed('/login');
                    _showDialogSuccess();
                  });
                });
              }
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
              color: myTheme.greenPrimary,
            ),
            child: Text(
              Languages.get('LBL_REGISTER'),
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        ),
        _loginAccountLabel(),
      ],
    );
  }

  Widget _loginAccountLabel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            Languages.get('LBL_HAVE_ACC'),
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
//              Navigator.of(context).pushReplacementNamed('/login');
            },
            child: Text(
              Languages.get('LBL_SIGNIN'),
              style: TextStyle(
                  color: myTheme.greenPrimary,
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }

  Widget _emailPasswordWidget() {
    teamBloc.teamNameController.text = '';
    teamBloc.teamCodeController.text = '';
    teamBloc.usernameController.text = '';
    teamBloc.passwordController.text = '';
    teamBloc.verifyPassController.text = '';
    teamBloc.firstnameController.text = '';
    teamBloc.lastnameController.text = '';
    return Column(
      children: <Widget>[
        myTheme.buildInputField(
            getWidth(context, 1, null),
            teamBloc.teamNameController,
            Languages.get('LBL_TEAM_NAME'),
            false,
            teamBloc.teamNameStream,
            '',
            TextInputType.text),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            myTheme.buildInputField(
                getWidth(context, 0.4, null),
                teamBloc.teamCodeController,
                Languages.get('LBL_TEAM_CODE'),
                false,
                teamBloc.teamCodeStream,
                '',
                TextInputType.text),
            myTheme.buildInputField(
                getWidth(context, 0.4, null),
                teamBloc.usernameController,
                Languages.get('LBL_USERNAME'),
                false,
                teamBloc.usernameStream,
                '',
                TextInputType.text),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            myTheme.buildInputField(
                getWidth(context, 0.4, null),
                teamBloc.passwordController,
                Languages.get('LBL_PASSWORD'),
                true,
                teamBloc.passwordStream,
                '',
                TextInputType.text),
            myTheme.buildInputField(
                getWidth(context, 0.4, null),
                teamBloc.verifyPassController,
                Languages.get('LBL_CONFIRM_PASSWORD'),
                true,
                teamBloc.verifyPassStream,
                '',
                TextInputType.text),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            myTheme.buildInputField(
                getWidth(context, 0.4, null),
                teamBloc.firstnameController,
                Languages.get('LBL_FIRST_NAME'),
                false,
                teamBloc.firstnameStream,
                '',
                TextInputType.text),
            myTheme.buildInputField(
                getWidth(context, 0.4, null),
                teamBloc.lastnameController,
                Languages.get('LBL_LAST_NAME'),
                false,
                teamBloc.lastnameStream,
                '',
                TextInputType.text),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: <Widget>[
            Positioned(
              top: -MediaQuery.of(context).size.height * .15,
              right: -MediaQuery.of(context).size.width * .4,
              child: BezierContainer(),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    myTheme.title(),
                    SizedBox(
                      height: 30,
                    ),
                    _emailPasswordWidget(),
                    SizedBox(
                      height: 20,
                    ),
                    _submitButton(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showDialogSuccess() {
    showDialog(
      context: context,
      child: SimpleDialog(
        title: Row(
          children: <Widget>[
            Icon(
              Icons.check,
              color: myTheme.greenPrimary,
              size: 25,
            ),
            Text(
              Languages.get('LBL_SIGN_UP_SUCCESS'),
              style: TextStyle(
                fontSize: 18,
                color: myTheme.greenPrimary,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(50.0, 20, 40, 20),
            child: Text(
              Languages.get('LBL_SIGN_UP_SUCCESS_NOTI'),
              style: TextStyle(
                fontSize: 13,
                color: myTheme.black,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  color: myTheme.greenPrimary,
                  child: Text(
                    Languages.get('LBL_OK'),
                    style: TextStyle(color: myTheme.white),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
