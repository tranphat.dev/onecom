import '../all_file.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  double height;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              FractionallySizedBox(
                heightFactor: height,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: SizedBox(),
                      ),
                      myTheme.title(),
                      SizedBox(
                        height: 50,
                      ),
                      myTheme.buildInputField(
                          getWidth(context, 1, null),
                          loginBloc.usernameController,
                          Languages.get('LBL_USERNAME'),
                          false,
                          loginBloc.nameStream,
                          '',
                          TextInputType.text),
                      myTheme.buildInputField(
                          getWidth(context, 1, null),
                          loginBloc.passController,
                          Languages.get('LBL_PASSWORD'),
                          true,
                          loginBloc.passStream,
                          '',
                          TextInputType.text),
                      _submitButton(),
                      GestureDetector(
                        onTap: () =>
                            Navigator.of(context).pushNamed('/forgotpass'),
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.centerRight,
                          child: Text(Languages.get('LBL_FORGOT_PASS'),
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500)),
                        ),
                      ),
                      _divider(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: _createAccountLabel(),
                      ),
                      Expanded(
                        flex: 2,
                        child: SizedBox(),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                  top: -MediaQuery.of(context).size.height * .15,
                  right: -MediaQuery.of(context).size.width * .4,
                  child: BezierContainer()),
            ],
          ),
        ),
      ),
    );
  }

  Widget _submitButton() {
    ProgressDialog pr;
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: Languages.get('LBL_PLEASE_WAIT'));
    return GestureDetector(
      onTap: () async {
        pr.show();
        bool result = await loginBloc.login();
        if (result) {
          Future.delayed(Duration(seconds: 0)).then((value) {
            pr.hide().whenComplete(() {
              Navigator.of(context).pushReplacementNamed('/homescreen');
            });
          });
        } else {
          Future.delayed(Duration(seconds: 0)).then((value) {
            pr.hide().whenComplete(() {
              _showDialogError();
            });
          });
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
          color: myTheme.greenPrimary,
        ),
        child: Text(
          Languages.get('LBL_SIGNIN'),
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text(Languages.get('LBL_OR')),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget _createAccountLabel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            Languages.get('LBL_DONT_HAVE_ACC'),
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () => Navigator.pushNamed(context, '/signup'),
            child: Text(
              Languages.get('LBL_SIGNUP'),
              style: TextStyle(
                  color: myTheme.greenPrimary,
                  fontSize: 15,
                  fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }

  void _showDialogError() {
    showDialog(
      context: context,
      child: SimpleDialog(
        title: Row(
          children: <Widget>[
            Icon(
              Icons.error_outline,
              color: Colors.red,
              size: 25,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                Languages.get('LBL_SIGN_IN_ERROR'),
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(50.0, 20, 40, 20),
            child: Text(
              (appState.log != '')
                  ? appState.log
                  : Languages.get('LBL_SIGN_IN_ERROR_NOTI'),
              style: TextStyle(
                fontSize: 13,
                color: myTheme.black,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  color: myTheme.greenPrimary,
                  child: Text(
                    Languages.get('LBL_OK'),
                    style: TextStyle(color: myTheme.white),
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
