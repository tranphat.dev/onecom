import '../all_file.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), () => onDoneLoading());
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(seconds: 2),
      alignment: Alignment.topCenter,
      decoration: new BoxDecoration(
        color: Colors.white,
      ),
      padding: EdgeInsets.symmetric(horizontal: 70, vertical: 0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: getPercentHeight(context, 0.37, null),
          ),
          Image.asset(
            'assets/img/flash_screen.jpeg',
          ),
          SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }

  onDoneLoading() async {
    appState.prefs = await SharedPreferences.getInstance();

    if (appState.prefs.getString('username') != null &&
        appState.prefs.getString('password') != null) {
      bool result = await api.login(
          name: appState.prefs.getString('username'),
          pass: appState.prefs.getString('password'));
      if (result) {
        Navigator.of(context).pushReplacementNamed('/homescreen');
      } else
        Navigator.of(context).pushReplacementNamed('/login');
    } else
      Navigator.of(context).pushReplacementNamed('/login');
  }
}
