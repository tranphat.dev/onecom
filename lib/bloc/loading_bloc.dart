import '../all_file.dart';

class LoadingBloc {
  bool isLoading = false;
  StreamController _loadingController = new StreamController.broadcast();

  Stream get loadingStream => _loadingController.stream;

  void dispose() {
    _loadingController.close();
  }

  void start() {
    isLoading = true;
    _loadingController.sink.add('start');
  }

  void end() {
    isLoading = false;
    _loadingController.sink.add('end');
  }
}

final loadingBloc = LoadingBloc();
