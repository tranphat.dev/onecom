import '../all_file.dart';

class ReloadBloc {
  StreamController _reloadController = new StreamController.broadcast();

  Stream get reloadStream => _reloadController.stream;

  void reload() {
    _reloadController.sink.add('F5');
  }


  void dispose() {
    _reloadController.close();
  }
}

final reloadBloc = ReloadBloc();
