import 'dart:async';

import '../all_file.dart';

class PanelBloc {
  bool _showDialog = false;

  StreamController _panelController = new StreamController.broadcast();

  Stream get panelStream => _panelController.stream;

  bool get isShowDialog => _showDialog;

  void setShowDialog(bool value) {
    _showDialog = value;
    _panelController.sink.add(value);
  }

  void dispose() {
    _panelController.close();
  }
}
