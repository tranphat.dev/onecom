import 'package:one_com/bloc/time_bloc.dart';

import '../all_file.dart';

class ShiftworkBloc {
  StreamController _shiftworkController = new StreamController.broadcast();
  Stream get shiftworkStream => _shiftworkController.stream;

  StreamController _beginTimeController = new StreamController.broadcast();
  Stream get beginTimeStream => _beginTimeController.stream;

  StreamController _endTimeController = new StreamController.broadcast();
  Stream get endTimeStream => _endTimeController.stream;

  TextEditingController shiftworkController = TextEditingController();
  DateTime beginTimeController;
  DateTime endTimeController;

  bool validate() {
    return true;
  }

  Future<bool> create() async {
    var result = await api.createShiftwork(
      shiftworkName: shiftworkController.text,
      begin: timeBloc.selectedTimeBegin,
      end: timeBloc.selectedTimeEnd,
      team_id: appState.teamInfo.id,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> update(String id) async {
    var result = await api.updateShiftwork(
      id: id,
      begin: timeBloc.selectedTimeBegin,
      end: timeBloc.selectedTimeEnd,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> delete(String id) async {
    var result = await api.deleteShiftwork(
      id: id,
    );
    if (result) api.reload();
    return result;
  }

  void dispose() {
    _shiftworkController.close();
    _beginTimeController.close();
    _endTimeController.close();
  }
}
final shiftworkBloc = ShiftworkBloc();
