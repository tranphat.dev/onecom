import '../all_file.dart';

class UserBloc {
  StreamController _firstNameController = new StreamController.broadcast();

  Stream get firstNameStream => _firstNameController.stream;

  StreamController _lastNameController = new StreamController.broadcast();

  Stream get lastNameStream => _lastNameController.stream;

  StreamController _dobController = new StreamController.broadcast();

  Stream get dobStream => _dobController.stream;

  StreamController _phoneController = new StreamController.broadcast();

  Stream get phoneStream => _phoneController.stream;

  StreamController _emailController = new StreamController.broadcast();

  Stream get emailStream => _emailController.stream;

  StreamController _jobTitleController = new StreamController.broadcast();

  Stream get jobTitleStream => _jobTitleController.stream;

  StreamController _usernameController = new StreamController.broadcast();

  Stream get usernameStream => _usernameController.stream;

  StreamController _passwordController = new StreamController.broadcast();

  Stream get passwordStream => _passwordController.stream;

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  DateTime dobController;
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController jobTitleController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Future<bool> create() async {
    var result = await api.createUser(
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      dateOfBirth: dateBloc.selectedDate,
      email: emailController.text,
      phone: phoneController.text,
      avataUrl: fileBloc.image,
      jobTitle: jobTitleController.text,
      username: usernameController.text,
      password: passwordController.text,
      team_id: appState.teamInfo.id,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> update(String id) async {
    var result = await api.updateUser(
      id: id,
      firstName: firstNameController.text,
      lastName: lastNameController.text,
      dateOfBirth: dateBloc.selectedDate,
      email: emailController.text,
      phone: phoneController.text,
      avataUrl: fileBloc.image,
      jobTitle: jobTitleController.text,
    );
    if (result) api.reload();
    return result;
  }

  Future<bool> delete(String id) async {
    var result = await api.deleteUser(
      id: id,
    );
    if (result) api.reload();
    return result;
  }

  bool validate() {
    bool result = true;
    String empty = Languages.get('LBL_NOT_EMPTY');
    String malformed = Languages.get('LBL_MALFORMED');
    String characterError = Languages.get('LBL_RANGE_CHARACTER');
    String characterUpper = Languages.get('LBL_UPPERCASE_CHARACTER');
    // Validate firstName
    if (firstNameController.text == '') {
      _firstNameController.sink.addError(empty);
      result = false;
    } else {
      _firstNameController.sink.add('');
    }
    // Validate lastName
    if (lastNameController.text == '') {
      _lastNameController.sink.addError(empty);
      result = false;
    } else {
      _lastNameController.sink.add('');
    }
    // Validate email
    if (!emailController.text.contains('@')) {
      _emailController.sink.addError(malformed);
      result = false;
    } else {
      _emailController.sink.add('');
    }
    // Validate jobTitle
    if (jobTitleController.text == '') {
      _jobTitleController.sink.addError(empty);
      result = false;
    } else {
      _jobTitleController.sink.add('');
    }
    // Validate username
    String username = usernameController.text;
    if (username == '') {
      _usernameController.sink.addError(empty);
      result = false;
    } else if (username.length < 6 || username.length > 16) {
      _usernameController.sink.addError(characterError);
      result = false;
    } else if (username != username.toLowerCase()) {
      _usernameController.sink.addError(characterUpper);
      result = false;
    } else if (username.contains('~') ||
        username.contains('`') ||
        username.contains('!') ||
        username.contains('@') ||
        username.contains('#') ||
        username.contains('%') ||
        username.contains('^') ||
        username.contains('&') ||
        username.contains('*') ||
        username.contains('(') ||
        username.contains(')') ||
        username.contains('+') ||
        username.contains('-') ||
        username.contains('_') ||
        username.contains('=')) {
      _usernameController.sink.addError(malformed);
      result = false;
    } else {
      _usernameController.add('');
    }

//     Validate password
    String password = passwordController.text;
    if (password == '') {
      _passwordController.sink.addError(empty);
      result = false;
    } else if (password.length < 6 || password.length > 16) {
      _passwordController.sink.addError(characterError);
      result = false;
    } else {
      _passwordController.sink.add('');
    }
    return result;
  }

  bool validateEdit() {
    bool result = true;
    String empty = Languages.get('LBL_NOT_EMPTY');
    String malformed = Languages.get('LBL_MALFORMED');
    // Validate firstName
    if (firstNameController.text == '') {
      _firstNameController.sink.addError(empty);
      result = false;
    } else {
      _firstNameController.sink.add('');
    }
    // Validate lastName
    if (lastNameController.text == '') {
      _lastNameController.sink.addError(empty);
      result = false;
    } else {
      _lastNameController.sink.add('');
    }
    // Validate email
    if (!emailController.text.contains('@')) {
      _emailController.sink.addError(malformed);
      result = false;
    } else {
      _emailController.sink.add('');
    }
    // Validate jobTitle
    if (jobTitleController.text == '') {
      _jobTitleController.sink.addError(empty);
      result = false;
    } else {
      _jobTitleController.sink.add('');
    }
    return result;
  }

  void dispose() {
    _firstNameController.close();
    _lastNameController.close();
    _dobController.close();
    _phoneController.close();
    _emailController.close();
    _jobTitleController.close();
    _usernameController.close();
    _passwordController.close();
  }
}

final UserBloc userBloc = UserBloc();
