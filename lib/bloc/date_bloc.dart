import 'package:intl/intl.dart';

import '../all_file.dart';

class DateBloc {
  var formatDate = DateFormat('d-MM-yyyy');

  DateTime _selectedDate;

  StreamController _changeController = new StreamController.broadcast();

  Stream get changeStream => _changeController.stream;

  DateTime get selectedDate =>
      (_selectedDate != null) ? _selectedDate : DateTime.now();

  String get getSelectedDateString => getDisplayDateTime(selectedDate);

  String get getDbSelectedDateString => selectedDate.toString();

  void dispose() {
    _changeController.close();
  }

  void setSelectedDate(DateTime date) {
    _selectedDate = date;
    _changeController.sink.add('Change');
  }
}

final dateBloc = DateBloc();
