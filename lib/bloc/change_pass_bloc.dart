import '../all_file.dart';

class ChangePassBloc {
  StreamController _updateController = new StreamController.broadcast();

  Stream get updateStream => _updateController.stream;

  TextEditingController currentController = new TextEditingController();
  TextEditingController newController = new TextEditingController();
  TextEditingController confirmController = new TextEditingController();
  bool currentError = false, newError = false, confirmError = false, compareError = false;
  FocusNode nodeOne = FocusNode();
  FocusNode nodeTwo = FocusNode();
  FocusNode nodeThree = FocusNode();

  void reload() {
    _updateController.sink.add('Change');
  }

  void clear() {
    currentController.clear();
    newController.clear();
    confirmController.clear();
    currentError = false;
    newError = false;
    confirmError = false;
    compareError = false;
  }

  void dispose() {
    _updateController.close();
  }
}

final ChangePassBloc changePassBloc = ChangePassBloc();
