import '../all_file.dart';

class TeamBloc {
  StreamController _teamNameController = new StreamController.broadcast();

  Stream get teamNameStream => _teamNameController.stream;

  StreamController _teamCodeController = new StreamController.broadcast();

  Stream get teamCodeStream => _teamCodeController.stream;

  StreamController _teamEmailController = new StreamController.broadcast();

  Stream get teamEmailStream => _teamEmailController.stream;

  StreamController _teamPhoneController = new StreamController.broadcast();

  Stream get teamPhoneStream => _teamPhoneController.stream;

  StreamController _teamAddressController = new StreamController.broadcast();

  Stream get teamAddressStream => _teamAddressController.stream;

  StreamController _timeLimitController = new StreamController.broadcast();

  Stream get timeLimitStream => _timeLimitController.stream;

  StreamController _ipWifiController = new StreamController.broadcast();

  Stream get ipWifiStream => _ipWifiController.stream;

  TextEditingController teamNameController = TextEditingController();
  TextEditingController teamCodeController = TextEditingController();
  TextEditingController teamEmailController = TextEditingController();
  TextEditingController teamPhoneController = TextEditingController();
  TextEditingController teamAddressController = TextEditingController();
  TextEditingController timeLimitController = TextEditingController();
  TextEditingController ipWifiController = TextEditingController();

  // Create Team
  StreamController _usernameController = new StreamController.broadcast();

  Stream get usernameStream => _usernameController.stream;
  StreamController _passwordController = new StreamController.broadcast();

  Stream get passwordStream => _passwordController.stream;
  StreamController _verifyPassController = new StreamController.broadcast();

  Stream get verifyPassStream => _verifyPassController.stream;
  StreamController _firstnameController = new StreamController.broadcast();

  Stream get firstnameStream => _firstnameController.stream;
  StreamController _lastnameController = new StreamController.broadcast();

  Stream get lastnameStream => _lastnameController.stream;
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController verifyPassController = TextEditingController();
  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();

  Future<bool> create() async {
    var result = await api.createTeam(
      teamName: teamNameController.text,
      teamCode: teamCodeController.text,
      username: usernameController.text,
      password: passwordController.text,
      firstName: firstnameController.text,
      lastName: lastnameController.text,
    );

    return result;
  }

  Future<bool> update(String id) async {
    var result = await api.updateTeam(
      id: id,
      teamName: teamNameController.text,
      teamCode: teamCodeController.text,
      email: teamEmailController.text,
      phone: teamPhoneController.text,
      address: teamAddressController.text,
      timeLimit: (timeLimitController.text != '') ? int.parse(timeLimitController.text) : 0,
      ipWifi: ipWifiController.text,
    );

    if (result) api.reload();
    return result;
  }

  Future<bool> delete(String id) async {}

  bool validate() {
    var result = true;
    String empty = Languages.get('LBL_NOT_EMPTY');
    String malformed = Languages.get('LBL_MALFORMED');
    String characterError = Languages.get('LBL_RANGE_CHARACTER');
    String matchPassword = Languages.get('LBL_NOT_MATCH_PASSWORD');
    String characterUpper = Languages.get('LBL_UPPERCASE_CHARACTER');

    // Validate teamName
    if (teamNameController.text == '') {
      _teamNameController.sink.addError(empty);
      result = false;
    } else {
      _teamNameController.sink.add('');
    }
    // Validate teamCode
    if (teamCodeController.text == '') {
      _teamCodeController.sink.addError(empty);
      result = false;
    } else {
      _teamCodeController.sink.add('');
    }
    // Validate username
    String username = usernameController.text;
    if (username == '') {
      _usernameController.sink.addError(empty);
      result = false;
    } else if (username.length < 6 || username.length > 16) {
      _usernameController.sink.addError(characterError);
      result = false;
    } else if (username != username.toLowerCase()) {
      _usernameController.sink.addError(characterUpper);
      result = false;
    } else if (username.contains('~') ||
        username.contains('`') ||
        username.contains('!') ||
        username.contains('@') ||
        username.contains('#') ||
        username.contains('%') ||
        username.contains('^') ||
        username.contains('&') ||
        username.contains('*') ||
        username.contains('(') ||
        username.contains(')') ||
        username.contains('+') ||
        username.contains('-') ||
        username.contains('_') ||
        username.contains('=')) {
      _usernameController.sink.addError(malformed);
      result = false;
    } else {
      _usernameController.add('');
    }
    // Validate password
    String password = passwordController.text;
    String verifyPassword = verifyPassController.text;
    if (password == '') {
      _passwordController.sink.addError(empty);
      result = false;
    } else if (password.length < 6 || password.length > 16) {
      _passwordController.sink.addError(characterError);
      result = false;
    } else if (password != verifyPassword) {
      _verifyPassController.sink.addError(matchPassword);
      result = false;
    } else {
      _passwordController.sink.add('');
    }
    // Validate firstName
    if (firstnameController.text == '') {
      _firstnameController.sink.addError(empty);
      result = false;
    } else {
      _firstnameController.sink.add('');
    }
    // Validate lastName
    if (lastnameController.text == '') {
      _lastnameController.sink.addError(empty);
      result = false;
    } else {
      _lastnameController.sink.add('');
    }
    return result;
  }

  bool validateEdit() {
    var result = true;
    String empty = Languages.get('LBL_NOT_EMPTY');

    // Validate teamName
    if (teamNameController.text == '') {
      _teamNameController.sink.addError(empty);
      result = false;
    } else {
      _teamNameController.sink.add('');
    }
    // Validate teamCode
    if (teamCodeController.text == '') {
      _teamCodeController.sink.addError(empty);
      result = false;
    } else {
      _teamCodeController.sink.add('');
    }
    // Validate email
    if (teamEmailController.text == '') {
      _teamEmailController.sink.addError(empty);
      result = false;
    } else {
      _teamEmailController.sink.add('');
    }
    return result;
  }


  void dispose() {
    _teamNameController.close();
    _teamCodeController.close();
    _teamEmailController.close();
    _teamPhoneController.close();
    _teamAddressController.close();
    _timeLimitController.close();
    _ipWifiController.close();
    _usernameController.close();
    _passwordController.close();
    _verifyPassController.close();
    _firstnameController.close();
    _lastnameController.close();
  }
}

final TeamBloc teamBloc = TeamBloc();
