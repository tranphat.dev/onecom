import '../all_file.dart';

class ReportBloc {
  DateTime dateStart;
  DateTime dateEnd;

  List<ReportDetailModel> reportDetail;

  StreamController _dateController = new StreamController.broadcast();

  Stream get dateStream => _dateController.stream;
  StreamController _dataController = new StreamController.broadcast();

  Stream get dataStream => _dataController.stream;

  void setDatetime({DateTime from, DateTime to}) {
    if (from != null) dateStart = from;
    if (to != null) dateEnd = to;
    _dateController.sink.add('F5');
    updateDataReport();
  }

  void updateDataReport() {
    api.getShiftReport(
      team_id: appState.teamInfo.id,
      from: dateStart,
      to: dateEnd,
    ).then((value) {
      reportDetail = value;
      _dataController.sink.add('F5');
    });
  }

  void dispose() {
    _dateController.close();
    _dataController.close();
  }
}

final reportBloc = ReportBloc();
