import 'package:intl/date_symbol_data_local.dart';
import 'all_file.dart';

void main() => initializeDateFormatting().then((_) => runApp(new MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'OneCom',
      // Search and replace app name
      theme: new ThemeData(
        fontFamily: 'SFUIText',
        primarySwatch: Colors.green,
        primaryColor: myTheme.greenPrimary,
        accentColor: myTheme.greenPrimary,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          textTheme: TextTheme(
            title: TextStyle(
              color: Colors.black,
              fontSize: 24,
              fontWeight: FontWeight.w700,
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => SplashScreen(),
        '/login': (context) => LoginScreen(),
        '/signup': (context) => SignUpScreen(),
        '/forgotpass': (context) => ForgotScreen(),
        '/homescreen': (context) => HomeScreen(),
        '/teaminfo': (context) => TeamInfo(),
        '/viewnews': (context) => NewsPage(),
        '/manage': (context) => ManageScreen(),
        '/team_management': (context) => TeamManagement(),
        '/user_management': (context) => UserManagement(),
        '/news_management': (context) => NewsManagement(),
        '/shiftwork_management': (context) => ShiftworkManagement(),
        '/reports': (context) => Reports(),
        '/report': (context) => ReportDetail(),
      },
    );
  }
}
