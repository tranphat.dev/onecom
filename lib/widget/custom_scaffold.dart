import '../all_file.dart';

class CustomScaffold extends StatefulWidget {
  final Widget title;
  final Widget floatActionButton;
  final Widget body;
  final Color backgroundColor;
  final Color appBarColor;
  final bottomNavigationBar;
  final List<Widget> actions;
  final Widget leading;
  String label;


  CustomScaffold(
      {this.title,
      this.backgroundColor = Colors.white,
      this.floatActionButton,
      this.body,
      this.appBarColor,
      this.actions,
      this.bottomNavigationBar,
      this.leading,
      this.label,
      });

  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
          floatingActionButton: widget.floatActionButton,
          backgroundColor: widget.backgroundColor,
          bottomNavigationBar: widget.bottomNavigationBar,
          appBar: AppBar(
            backgroundColor: myTheme.greenPrimary,
            title: (widget.title != null) ? widget.title : _buildTitle(),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(myTheme.borderRadius),
              ),
            ),
            leading: widget.leading,
            actions: widget.actions,
          ),
          body: widget.body),
    );
  }

  Widget _buildTitle() {
    return Text(
      Languages.get(widget.label),
      style: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
        letterSpacing: 1.0,
        color: Colors.white,
      ),
    );
  }
}
