import '../all_file.dart';

enum FieldType {
  TextField,
  TextArea,
  Picker,
//  MemberPicker,
//  DatePicker,
//  DateTimePicker,
  CheckBox,
//  RelatedTo,
//  Address,
  Numberic,
}

class EditField {
  dynamic onChange;
  final String label;
  final FieldType type;
  dynamic controller;
  final dynamic init;
  final List<JsonObject> pickerList;
  final int remove;
  TextEditingController typeController;
  String initType;
  bool isAccountRelated;
  bool isContactRelated;
  bool required;
  String hintText;
  final bool isCallScreen;
  Color labelColor;
  Color textColor;

  EditField({
    this.controller,
    this.label,
    this.onChange,
    this.type,
    this.init,
    this.pickerList,
    this.initType,
    this.typeController,
    this.isAccountRelated = false,
    this.isContactRelated = true,
    this.required = false,
    this.hintText = '',
    this.remove,
    this.isCallScreen = false,
    this.textColor = Colors.black,
    this.labelColor,
  });
}

class EditRow extends StatefulWidget {
  final List<EditField> children;
  final bool isCallScreen;

  EditRow({this.children, this.isCallScreen = false});

  @override
  _EditRowState createState() => _EditRowState();
}

class _EditRowState extends State<EditRow> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.children.forEach((EditField editField) {
      if (editField.type == FieldType.CheckBox) {
        editField.controller = editField.init ?? false;
      } else {
        editField.controller.text = editField.init ?? '';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.children.length == 1) {
      return Padding(
        padding: EdgeInsets.only(
            top: (widget.isCallScreen) ? 0 : 5,
            bottom: (widget.isCallScreen) ? 0 : 5,
            left: (widget.isCallScreen) ? 0 : 10,
            right: (widget.isCallScreen) ? 0 : 10),
        child: _buildSingleRecord(widget.children[0]),
      );
    }
    return Container(
        padding: EdgeInsets.only(
            top: (widget.isCallScreen) ? 0 : 5,
            bottom: (widget.isCallScreen) ? 0 : 5,
            left: (widget.isCallScreen) ? 0 : 10,
            right: (widget.isCallScreen) ? 0 : 10),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: _buildSingleRecord(widget.children[0]),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 1,
              child: _buildSingleRecord(widget.children[1]),
            )
          ],
        ));
  }

  Widget _buildSingleRecord(EditField editField) {
    Widget field;
    List<JsonObject> listPick = List<JsonObject>();
    if (editField.pickerList != null) listPick.addAll(editField.pickerList);
    if (editField.remove != null) {
      listPick
          .removeWhere((item) => item.key == listPick[editField.remove].key);
    }
    switch (editField.type) {
      case FieldType.TextField:
        field = _buildInputField(editField, false);
        break;
//      case FieldType.Address:
//        field = _buildAddressField(editField);
//        break;
      case FieldType.TextArea:
        field = _buildInputField(editField, true);
        break;
      case FieldType.Picker:
        field = CustomPicker(
          textColor: editField.textColor,
          list: listPick,
          editField: editField,
        );
        break;
//      case FieldType.MemberPicker:
//        field = CustomMemberPicker(
//          textColor: editField.textColor,
//          initMember: editField.init,
//          resultController: editField.controller,
//        );
//        break;
//      case FieldType.RelatedTo:
//        field = CustomRelatedPicker(
//          onChange: editField.onChange,
//          isHaveAccount: editField.isAccountRelated,
//          isHaveContact: editField.isContactRelated,
//          idController: editField.controller,
//          initId: editField.init,
//          typeController: editField.typeController,
//          initType: editField.initType,
//        );
//        break;
//      case FieldType.DatePicker:
//        field = CustomDatePicker(
//          onChange: editField.onChange,
//          initDbDateTime: editField.init,
//          resultController: editField.controller,
//        );
//        break;
//      case FieldType.DateTimePicker:
//        field = CustomDateTimePicker(
//          editField: editField,
//        );
//        break;
      case FieldType.CheckBox:
        field = Theme(
          data: (editField.isCallScreen)
              ? Theme.of(context).copyWith(
                  unselectedWidgetColor: Colors.white,
                )
              : Theme.of(context),
          child: Container(
            child: Checkbox(
              activeColor: (editField.isCallScreen)
                  ? myTheme.greenPrimary
                  : myTheme.greenSecondary,
              value: editField.controller ?? false,
              onChanged: (value) {
                setState(() {
                  editField.controller = value;
                });
              },
            ),
          ),
        );
        break;
      case FieldType.Numberic:
        field = _buildNumberField(editField, false);
        break;
    }
    List<Widget> _children = [
      Row(
        children: <Widget>[
          Text(
            Languages.get(editField.label),
            style: TextStyle(
                color: editField.labelColor == null
                    ? myTheme.greenPrimary
                    : editField.labelColor,
                fontWeight: FontWeight.w500,
                fontSize: 20),
          ),
          (editField.required)
              ? Text(
                  '*',
                  style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                )
              : Container(),
        ],
      ),
      field,
    ];
    if (widget.isCallScreen) {

        return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  Languages.get(editField.label),
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                (editField.required)
                    ? Text(
                  '*',
                  style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w500,
                      fontSize: 20),
                )
                    : Container(),
              ],
            ),
            SizedBox(
              width: 200,
              child: field,
            )
          ],
        );

    } else {
      return (editField.type == FieldType.CheckBox)
          ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _children,
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _children,
            );
    }
  }

  Widget _buildInputField(EditField editField, bool _isTextArea) {
    Widget textField = TextField(
      maxLines: (_isTextArea) ? null : 1,
      controller: editField.controller,
      onChanged: (value) {
        if (editField.required) {
          editField.onChange(value);
          appState.validateBloc.checkValidate();
        }
      },
      textInputAction: TextInputAction.done,
      decoration: (editField.isCallScreen)
          ? InputDecoration(
              hintText: editField.hintText,
              hintStyle: TextStyle(
                color: Colors.white,
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: myTheme.greenPrimary, width: 0.5),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: myTheme.greenPrimary, width: 0.5),
              ),
            )
          : InputDecoration(
              hintText: editField.hintText,
              hintStyle: TextStyle(color: Colors.grey),
              focusedBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: myTheme.greenPrimary, width: 0.5),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: myTheme.greenPrimary, width: 0.5),
              ),
            ),
      style: TextStyle(
          color: (editField.isCallScreen) ? Colors.white : Colors.black,
          fontSize: 18),
    );

    if (_isTextArea) {
      return Container(
        child: new ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: 300.0,
          ),
          child: new Scrollbar(
            child: new SingleChildScrollView(
                scrollDirection: Axis.vertical,
                reverse: true,
                child: textField),
          ),
        ),
      );
    } else
      return textField;
  }

//  Widget _buildAddressField(EditField editField) {
//    return Theme(
//        data: ThemeData(),
//        child: CustomAddress(
//          controller: editField.controller,
//          apiKey: AppConfig.GOOGLE_API,
//          language: 'vi',
//          location: LatLng(10.8090903, 106.6827811),
//          radius: 30000,
//          onSelected: (Place place) async {
//            print(place.description);
//            final geolocation = await place.geolocation;
//            editField.onChange(geolocation.coordinates.latitude,
//                geolocation.coordinates.longitude);
//          },
//        ));
//  }

  Widget _buildNumberField(EditField editField, bool _isTextArea) {
    Widget textField = TextField(
      keyboardType: TextInputType.number,
      maxLines: (_isTextArea) ? null : 1,
      controller: editField.controller,
      onChanged: (value) {
        if (editField.required) {
          editField.onChange(value);
          appState.validateBloc.checkValidate();
        }
      },
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
        hintText: editField.hintText,
        hintStyle: TextStyle(color: Colors.grey),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: myTheme.greenPrimary, width: 0.5),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: myTheme.greenPrimary, width: 0.5),
        ),
      ),
      style: TextStyle(color: editField.textColor, fontSize: 18),
    );

    if (_isTextArea) {
      return Container(
        child: new ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: 300.0,
          ),
          child: new Scrollbar(
            child: new SingleChildScrollView(
                scrollDirection: Axis.vertical,
                reverse: true,
                child: textField),
          ),
        ),
      );
    } else
      return textField;
  }
}
