import '../all_file.dart';
import 'package:flutter/cupertino.dart';

class CustomTab extends StatefulWidget {
  final List<Widget> children;
  final List<String> label;
  final Widget informationContainer;
  VoidCallback onPressed;

  CustomTab({
    this.label,
    this.children,
    this.informationContainer,
  });

  @override
  _CustomTabState createState() => _CustomTabState();
}

class _CustomTabState extends State<CustomTab> {
  int sharedValue = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      padding: const EdgeInsets.only(top: 10.0),
      child: Column(
        children: <Widget>[
          widget.informationContainer ?? Container(),
          SizedBox(
            width: MediaQuery.of(context).size.width,

            child: CupertinoSegmentedControl<int>(
              borderColor: myTheme.greenSecondary,
              selectedColor: myTheme.greenSecondary,
              children: _buildLabel(),
              onValueChanged: (int val) {
                setState(() {
                  sharedValue = val;
                });
              },
              groupValue: sharedValue,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Expanded(
            child: _buildContent(sharedValue),
          ),
        ],
      ),
    );
  }

  Map<int, Widget> _buildLabel() {
    Map<int, Widget> result = Map<int, Widget>();
    int index = 0;
    widget.label.forEach((label) {
      result[index] = Container(
        padding: const EdgeInsets.only(
            left: 15.0, top: 7.0, right: 15.0, bottom: 7.0),
        child: Text(label,style: TextStyle(fontSize: 16),),
      );
      index++;
    });

    return result;
  }

  Widget _buildContent(int index) {
    return SingleChildScrollView(child: widget.children[index]);
  }
}
