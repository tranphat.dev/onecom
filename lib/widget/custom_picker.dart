import '../all_file.dart';
import 'package:flutter/cupertino.dart';

class CustomPicker extends StatefulWidget {
  final List<JsonObject> list;
  final EditField editField;
  final bool isSettingScreen;
  Color textColor;
  double size;
  bool isButton;

  CustomPicker(
      {this.list,
      this.editField,
      this.isSettingScreen = false,
      this.textColor = Colors.black,
      this.size = 18,
      this.isButton = false});

  @override
  _CustomPickerState createState() => _CustomPickerState();
}

class _CustomPickerState extends State<CustomPicker> {
  JsonObject _selectedObject = JsonObject('', '');
  JsonObject _tempObject = JsonObject('', '');
  TextEditingController _controller = TextEditingController();
  int _selectedIndex = 0;

  @override
  void initState() {
    if (widget.editField.init != null && widget.editField.init != '') {
      for (int i = 0; i < widget.list.length; i++) {
        if (widget.list[i].key == widget.editField.init) {
          _selectedObject = widget.list[i];
          _selectedIndex = i;
        }
      }
    }

    if (_selectedObject.key == '') {
      _selectedObject = widget.list[0];
      _controller.text = Languages.get('LBL_DEFAULT_DROPDOWN');
      widget.editField.controller.text = '';
    } else {
      _controller.text = _selectedObject.value;
      widget.editField.controller.text = _selectedObject.key;
    }
    _tempObject = _selectedObject;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isButton) {
      return MaterialButton(
        padding: EdgeInsets.zero,
        onPressed: () => showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return _buildBottomPicker();
          },
        ),
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            children: <Widget>[
              Icon(
                Icons.calendar_today,
                color: Colors.black,
                size: 15,
              ),
              Container(
                  width: 100,
                  padding: EdgeInsets.only(left: 6),
                  child: Center(
                    child: TextField(
                      onTap: () => showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return _buildBottomPicker();
                        },
                      ),
                      controller: _controller,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 5),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: myTheme.greenPrimary, width: 0.5),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: myTheme.greenPrimary, width: 0.5),
                        ),
                      ),
                      readOnly: true,
                      style: TextStyle(
                          color: widget.textColor, fontSize: widget.size),
                    ),
                  )),
              Icon(
                Icons.arrow_drop_down,
                color: Colors.black,
                size: 15,
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: TextField(
          onTap: () => showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return _buildBottomPicker();
            },
          ),
          controller: _controller,
          textAlign:
              (widget.editField.isCallScreen) ? TextAlign.end : TextAlign.start,
          decoration: (widget.editField.isCallScreen)
              ? InputDecoration(border: InputBorder.none)
              : InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: myTheme.greenPrimary, width: 0.5),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: (widget.isSettingScreen)
                        ? BorderSide.none
                        : BorderSide(
                            color: myTheme.greenPrimary, width: 0.5),
                  ),
                ),
          readOnly: true,
          style: TextStyle(
              color: (widget.editField.isCallScreen)
                  ? Colors.white
                  : (widget.editField.controller.text == '')
                      ? Colors.grey
                      : widget.textColor,
              fontSize: widget.size),
        ),
      );
    }
  }

  Widget _buildBottomPicker() {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _selectedIndex);

    return Container(
      height: 250,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(myTheme.borderRadius / 2),
          topLeft: Radius.circular(myTheme.borderRadius / 2),
        ),
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              MaterialButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  Languages.get('LBL_CANCEL'),
                  style: TextStyle(color: Colors.grey, fontSize: widget.size),
                ),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _selectedObject = _tempObject;
                    _controller.text = _selectedObject.value;
                    widget.editField.controller.text = _selectedObject.key;

                    if (widget.editField.onChange != null) {
                      widget.editField.onChange(_selectedObject.key);
                      appState.validateBloc.checkValidate();
                    }
//
//                    dashBoardBloc.refresh();
                  });
                  Navigator.pop(context);
                },
                child: Text(
                  Languages.get('LBL_DONE'),
                  style: TextStyle(color: Colors.blue, fontSize: widget.size),
                ),
              ),
            ],
          ),
          Expanded(
            child: CupertinoPicker(
              itemExtent: 50,
              scrollController: scrollController,
              backgroundColor: CupertinoColors.white,
              onSelectedItemChanged: (int index) {
                setState(() {
                  _tempObject = widget.list[index];
                  _selectedIndex = index;
                });
              },
              children:
                  new List<Widget>.generate(widget.list.length, (int index) {
                return new Center(
                  child: new Text(
                    widget.list[index].value,
                    style: TextStyle(
                        color: widget.textColor, fontSize: widget.size),
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
