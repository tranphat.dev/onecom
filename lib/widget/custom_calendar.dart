import '../all_file.dart';
import 'package:intl/intl.dart';

class CustomCalendar extends StatefulWidget {
  CustomCalendar({Key key}) : super(key: key);

  @override
  _CustomCalendarState createState() => _CustomCalendarState();
}

class _CustomCalendarState extends State<CustomCalendar> {
  DateTime _currentDate;
  DateTime _targetDate;
  String _currentMonth = '';

  EventList<Event> _markedDateMap = EventList<Event>();

  List<String> _ledgerList = List<String>();

  /// for bottom list UI
  List<String> _ledgerListOfSelectedDate = List<String>();

  void selectDate(
    DateTime date,
  ) {
    _ledgerListOfSelectedDate.clear();
    setState(() {
      _currentDate = date;
      _targetDate = DateTime(date.year, date.month);
      _currentMonth = DateFormat.yMMM().format(date);
    });
  }

  @override
  void initState() {
    super.initState();
    _currentDate = DateTime.now();
    _targetDate = DateTime.now();
    _currentMonth = DateFormat.yMMM().format(_currentDate);
  }

  @override
  Widget build(BuildContext context) {
    void onDatePressed() async {
      int year = this._currentDate.year;
      int prevDate = year - 100;
      int lastDate = year + 10;
      DateTime pickDate = await showDatePicker(
        context: context,
        initialDate: this._currentDate,
        firstDate: DateTime(prevDate),
        lastDate: DateTime(lastDate),
      );
      if (pickDate != null) {
        this.selectDate(pickDate);
      }
    }

    return SafeArea(
      top: false,
      child: Container(
        margin: EdgeInsets.only(left: 5, right: 5),
        child: Column(
          children: <Widget>[
            DateSelector(
              date: this._currentMonth,
              onDatePressed: onDatePressed,
            ),
            renderCalendar(
              context: context,
              onCalendarChanged: (DateTime date) {
                this.setState(() {
                  _currentMonth = DateFormat.yMMM().format(date);
                  _targetDate = date;
                });
              },
              onDayPressed: (DateTime date, List<Event> events) {
                this.selectDate(date);
              },
              markedDateMap: _markedDateMap,
              currentDate: _currentDate,
              targetDate: _targetDate,
            ),
            Divider(
              color: Colors.grey,
              indent: 10,
              endIndent: 10,
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              itemCount: _ledgerListOfSelectedDate.length,
              itemBuilder: (BuildContext context, int index) {
                return Text('$index');
              },
            ),
          ],
        ),
      ),
    );
  }
}

Widget renderCalendar({
  BuildContext context,
  Function onCalendarChanged,
  Function onDayPressed,
  EventList<Event> markedDateMap,
  DateTime currentDate,
  DateTime targetDate,
}) {
  return CalendarCarousel<Event>(
    onCalendarChanged: onCalendarChanged,
    onDayPressed: onDayPressed,

    /// make calendar to be scrollable together with its screen
    customGridViewPhysics: NeverScrollableScrollPhysics(),

    /// marked Date
    markedDatesMap: markedDateMap,
    markedDateShowIcon: true,
    markedDateIconMaxShown: 1,
    markedDateIconBuilder: (event) {
      return renderMarkedIcon(
          color: Theme.of(context).accentColor, context: context);
    },

    /// selected date
    selectedDayButtonColor: myTheme.greenPrimary,
    selectedDateTime: currentDate,
    selectedDayTextStyle: TextStyle(
      color: Colors.white,
    ),

    pageSnapping: true,
    weekFormat: false,
    showHeader: false,
    thisMonthDayBorderColor: Colors.grey,
    height:
        MediaQuery.of(context).orientation == Orientation.portrait ? 380 : 490,
    childAspectRatio:
        MediaQuery.of(context).orientation == Orientation.portrait ? 1.0 : 1.5,
    targetDateTime: targetDate,

    /// weekday
    weekdayTextStyle: TextStyle(color: myTheme.greenPrimary),
    weekendTextStyle: TextStyle(color: myTheme.greenPrimary),
    daysTextStyle: TextStyle(color: Theme.of(context).textTheme.title.color),
    todayBorderColor: Colors.green,
    todayTextStyle: TextStyle(
      color: Theme.of(context).primaryColor,
    ),
    todayButtonColor: Colors.grey.withOpacity(0.3),
    minSelectedDate: DateTime(1970, 1, 1),
    maxSelectedDate: DateTime.now().add(Duration(days: 3650)),
  );
}

Widget renderMarkedIcon({Color color, BuildContext context}) {
  return Container(
    child: Stack(
      children: <Widget>[
        Positioned(
          child: CustomPaint(painter: DrawCircle(color: color)),
          top: 7,
          right: MediaQuery.of(context).orientation == Orientation.portrait
              ? 0
              : 15,
          height: 5,
          width: 5,
        )
      ],
    ),
  );
}

class DrawCircle extends CustomPainter {
  Paint _paint;

  DrawCircle({Color color}) {
    _paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(0.0, 0.0), 3.0, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
