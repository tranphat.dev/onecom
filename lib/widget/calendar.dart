import 'package:flutter/material.dart';

import '../all_file.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> with TickerProviderStateMixin {
  Map<DateTime, List> _events = Map<DateTime, List>();
  Map<DateTime, List> _visibleEvents;
  CalendarController _calendarController;
  List<bool> isSelected = [false, false, false, false];

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      _visibleEvents = Map.fromEntries(
        _events.entries.where(
          (entry) =>
              entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        StreamBuilder(
            stream: reloadBloc.reloadStream,
            builder: (context, snapshot) {
              _events.clear();
              appState.listShiftRegis.forEach((element) {
                (_events[DateTime.parse(element.date)] != null)
                    ? _events[DateTime.parse(element.date)].add(element)
                    : _events[DateTime.parse(element.date)] = [element];
              });
              _visibleEvents = _events;
              return CustomOverlayWidget(
                margin: EdgeInsets.all(5),
                borderRadius: myTheme.borderRadius / 2,
                child: _buildTableCalendar(),
              );
            }),
      ],
    );
  }

  Widget _buildTableCalendar() {
    return TableCalendar(
      calendarController: _calendarController,
      rowHeight: 60,
      locale: (appState.getLanguage == 'vn') ? 'vi_VN' : 'en_US',
      events: _visibleEvents,
      startingDayOfWeek: StartingDayOfWeek.monday,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: 'Month',
        CalendarFormat.twoWeeks: '2 weeks',
        CalendarFormat.week: 'Week',
      },
      calendarStyle: CalendarStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.black),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
      ),
      builders: CalendarBuilders(
        outsideDayBuilder: (context, date, _) => Container(),
        outsideWeekendDayBuilder: (context, date, _) => Container(),
        weekendDayBuilder: (context, date, events) => _buildDay(date, events),
        selectedDayBuilder: (context, date, events) => _buildDay(date, events),
        todayDayBuilder: (context, date, events) => _buildDay(date, events),
        dayBuilder: (context, date, events) => _buildDay(date, events),
        markersBuilder: (context, date, events, holidays) => <Widget>[],
      ),
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  Widget _buildDay(DateTime date, events) {
    final children = <Widget>[];
    bool toDay = SimpleUtil.dayIsEqual(
        date.millisecondsSinceEpoch, DateTime.now().millisecondsSinceEpoch);
    bool afterToDay = date.isAfter(DateTime.now());
    bool beforeToDay = date.isBefore(DateTime.now());
    Color color;
    // Build day
    if (beforeToDay) {
      children.add(
        FractionallySizedBox(
          alignment: Alignment.topCenter,
          heightFactor: 0.7,
          child: Center(
            child: Text(
              '${date.day}',
              style: TextStyle(
                color: myTheme.gray,
              ),
            ),
          ),
        ),
      );
    } else
      children.add(
        FractionallySizedBox(
          alignment: Alignment.topCenter,
          heightFactor: 0.7,
          child: Center(
            child: Text(
              '${date.day}',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: myTheme.greenPrimary,
              ),
            ),
          ),
        ),
      );
    if (toDay) {
      children.add(
        FractionallySizedBox(
          heightFactor: 0.7,
          alignment: Alignment.topCenter,
          child: Center(
            child: ClipRRect(
              borderRadius: new BorderRadius.circular(100),
              child: Container(
                width: 30,
                height: 30,
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                color: myTheme.greenPrimary,
                child: Center(
                  child: Text(
                    '${DateTime.now().day}',
                    textAlign: TextAlign.center,
                    style: TextStyle()
                        .copyWith(fontSize: 15.0, color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }

    // Build shift
    if (events != null) {
      List<Widget> _child = List();
      events.forEach((event) {
        if (beforeToDay) {
          if (event.isCheckin == 1 && event.isCheckout == 1)
            color = myTheme.gray;
          else
            color = Colors.redAccent;
        } else if (event.shiftworkName == 'All Day')
          color = Colors.blueAccent;
        else
          color = myTheme.greenSecondary;

        if (toDay) {
          if (event.isCheckin == 1 && event.isCheckout == 1)
            color = myTheme.gray;
          else if (event.shiftworkName == 'All Day')
            color = Colors.blueAccent;
          else
            color = myTheme.greenSecondary;
        }

        _child.add(
          InkWell(
            onTap: () {
              checkinBloc.setDateRegis(date);
              if (afterToDay || toDay) {
                appState.listShiftwork.forEach((element) {
                  if (element.id == event.shiftwork_id)
                    checkinBloc.setShift(element);
                });
                if (event.isCheckin == 0) _showDialogShiftwork();
              }
            },
            child: Container(
              margin: EdgeInsets.only(top: 10),
              height: 18,
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                  color: color),
              child: Container(
                width: 50,
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
                child: Center(
                  child: Text('${event.shiftworkName}',
                      style: TextStyle()
                          .copyWith(color: Colors.white, fontSize: 11),
                      overflow: TextOverflow.ellipsis),
                ),
              ),
            ),
          ),
        );
      });
      children.add(FractionallySizedBox(
        heightFactor: 0.5,
        alignment: Alignment.bottomCenter,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: _child,
          ),
        ),
      ));
    } else
      children.add(FractionallySizedBox(
        heightFactor: 0.5,
        alignment: Alignment.bottomCenter,
      ));

    return GestureDetector(
      onTap: () {
        checkinBloc.setDateRegis(date);
        if (afterToDay || toDay) {
          if (events != null) {
            appState.listShiftwork.forEach((element) {
              if (element.id == events[0].shiftwork_id)
                checkinBloc.setShift(element);
            });
          }
          if (events == null)
            _showDialogShiftwork();
          else if (events[0].isCheckin == 0) _showDialogShiftwork();
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 1, vertical: 2),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey, width: 0.2)),
        child: Stack(
          fit: StackFit.expand,
          children: children,
        ),
      ),
    );
  }

  Widget _buildOptionShiftwork(int index) {
    ConfigModel model = appState.listShiftwork[index];
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.fromLTRB(20, 8, 20, 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: (checkinBloc.selectedShift == model)
              ? myTheme.greenSecondary
              : myTheme.white,
        ),
        child: Center(
          child: Text(
            model.shiftworkName,
            style: TextStyle(
              fontSize: 18,
              color: (checkinBloc.selectedShift == model)
                  ? myTheme.white
                  : myTheme.black,
            ),
          ),
        ),
      ),
      onTap: () {
//        regisCalendarBloc.clearShift();
        checkinBloc.setTempShift(model);
        reloadBloc.reload();
      },
    );
  }

  void _showDialogShiftwork() {
    showDialog(
        context: context,
        child: new SimpleDialog(
          title: new Text(
            Languages.get('LBL_SELECT_SHIFTWORK'),
            style: TextStyle(
                fontSize: 20,
                color: myTheme.greenSecondary,
                fontWeight: FontWeight.bold),
          ),
          children: <Widget>[
            StreamBuilder(
                stream: reloadBloc.reloadStream,
                builder: (context, snapshot) {
                  return Container(
                    height: 200,
                    width: 200,
                    child: ListView.builder(
                      itemCount: appState.listShiftwork.length,
                      itemExtent: 50,
                      itemBuilder: (context, index) =>
                          _buildOptionShiftwork(index),
                    ),
                  );
                }),
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: myTheme.greenPrimary,
                    child: Text(
                      Languages.get('LBL_SAVE'),
                      style: TextStyle(color: myTheme.white),
                    ),
                    onPressed: () {
                      if (checkinBloc.selectedShift != null) {
                        checkinBloc.delete();
                        checkinBloc.create();
                        checkinBloc.clearShift();
                        Navigator.pop(context);
                      } else {
                        if (checkinBloc.getModelSelected() != null) {
                          checkinBloc.cancelShift();
                          Navigator.pop(context);
                        } else
                          Navigator.pop(context);
                      }
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(40),
                    ),
                    color: Colors.redAccent,
                    child: Text(
                      Languages.get('LBL_CANCEL'),
                      style: TextStyle(
                        color: myTheme.white,
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        checkinBloc.clearShift();
                        Navigator.pop(context);
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _buildCheckinButton() {
    return MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(40),
      ),
      color: myTheme.greenPrimary,
      child: Text(
        Languages.get('LBL_CHECKIN'),
        style: TextStyle(color: myTheme.white),
      ),
      onPressed: () {
        setState(() {
          checkinBloc.checkin();
        });
      },
    );
  }
}
