import 'package:one_com/bloc/time_bloc.dart';

import '../all_file.dart';
import 'package:flutter/cupertino.dart';

class CustomTimeEndPicker extends StatefulWidget {
  @override
  _CustomTimeEndPickerState createState() => _CustomTimeEndPickerState();
}

class _CustomTimeEndPickerState extends State<CustomTimeEndPicker> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        DatePicker.showTimePicker(
          context,
          showTitleActions: true,
          locale: LocaleType.vi,
          theme: DatePickerTheme(
              headerColor: Colors.white,
              backgroundColor: Colors.white,
              itemStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                  fontSize: 18),
              cancelStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
              doneStyle: TextStyle(
                  color: myTheme.greenPrimary,
                  fontSize: 18,
                  fontWeight: FontWeight.w500)),
          onConfirm: (date) {
            timeBloc.setSelectedTimeEnd(date);
          },
        );
      },
      child: StreamBuilder(
          stream: timeBloc.changeTimeEndStream,
          builder: (context, snapshot) {
            return Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  (timeBloc.selectedTimeEnd == null)
//                      ? Languages.get('LBL_SELECT_TIME')
                      ? timeBloc.setTimeEnd()
                      : timeBloc.getSelectedTimeEndString(),
                  textAlign: TextAlign.end,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                  ),
                )
              ],
            );
          }),
    );
  }
}
