import '../all_file.dart';

class CustomOverlayWidget extends StatefulWidget {
  final Widget child;
  final double borderRadius;
  final Color backgroundColor;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final double height;
  final double width;
  final double maxHeight;
  final bool fullRadius;
  final onTap;
  final bool isDelete;

  CustomOverlayWidget(
      {this.child,
      this.borderRadius = 20.0,
      this.backgroundColor = Colors.white,
      this.padding,
      this.margin = const EdgeInsets.all(5),
      this.height,
      this.width,
      this.maxHeight,
      this.fullRadius = true,
      this.onTap,
      this.isDelete = false});

  @override
  _CustomOverlayWidgetState createState() => _CustomOverlayWidgetState();
}

class _CustomOverlayWidgetState extends State<CustomOverlayWidget>
    with SingleTickerProviderStateMixin {
  double _scale;
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.onTap != null) {
      _scale = 1 - _controller.value;
      return GestureDetector(
        onTap: widget.onTap,
        onTapDown: _onTapDown,
        onTapUp: _onTapUp,
        child: Transform.scale(
          scale: _scale,
          child: Container(
            constraints: (widget.maxHeight != null)
                ? BoxConstraints(
                    maxHeight: widget.maxHeight,
                  )
                : null,
            height: widget.height,
            width: widget.width,
            padding: widget.padding,
            margin: widget.margin,
            decoration: BoxDecoration(
              borderRadius: (widget.fullRadius)
                  ? BorderRadius.circular(widget.borderRadius)
                  : BorderRadius.vertical(
                      top: Radius.circular(widget.borderRadius)),
              color: widget.backgroundColor,
              boxShadow: [
                BoxShadow(
                  color: (widget.isDelete) ? Colors.redAccent : myTheme.greenSecondary,
                  blurRadius: 2.0,
                  spreadRadius: 0.5,
                  offset: Offset(
                    0,
                    2.0,
                  ),
                ),
              ],
            ),
            child: widget.child,
          ),
        ),
      );
    } else {
      return Container(
        constraints: (widget.maxHeight != null)
            ? BoxConstraints(
                maxHeight: widget.maxHeight,
              )
            : null,
        height: widget.height,
        width: widget.width,
        padding: widget.padding,
        margin: widget.margin,
        decoration: BoxDecoration(
          borderRadius: (widget.fullRadius)
              ? BorderRadius.circular(widget.borderRadius)
              : BorderRadius.vertical(
                  top: Radius.circular(widget.borderRadius)),
          color: widget.backgroundColor,
          boxShadow: [
            BoxShadow(
              color: myTheme.greenSecondary,
              blurRadius: 2.0,
              spreadRadius: 0.5,
              offset: Offset(
                0,
                2.0,
              ),
            ),
          ],
        ),
        child: widget.child,
      );
    }
  }

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }
}
